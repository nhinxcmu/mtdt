//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DUONG
    {
        public DUONG()
        {
            this.SOGHIs = new HashSet<SOGHI>();
            this.KHACHHANGs = new HashSet<KHACHHANG>();
        }
    
        public int IDDUONG { get; set; }
        public Nullable<int> IDKHOMAP { get; set; }
        public string MAKHUVUC { get; set; }
        public string TENDUONG { get; set; }
    
        public virtual KHUVUC KHUVUC { get; set; }
        public virtual ICollection<SOGHI> SOGHIs { get; set; }
        public virtual ICollection<KHACHHANG> KHACHHANGs { get; set; }
    }
}
