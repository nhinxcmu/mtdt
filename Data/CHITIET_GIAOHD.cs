//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHITIET_GIAOHD
    {
        public int IDCTGHD { get; set; }
        public Nullable<int> IDGIAOHD { get; set; }
        public string MASOGHI { get; set; }
        public string TUSOSERI { get; set; }
        public string DENSOSERI { get; set; }
        public Nullable<int> GIAO_HD { get; set; }
        public Nullable<decimal> GIAO_SOTIEN { get; set; }
        public Nullable<int> XOACHINH_HD { get; set; }
        public Nullable<decimal> XOACHINH_SOTIEN { get; set; }
    
        public virtual GIAOHOADON GIAOHOADON { get; set; }
        public virtual SOGHI SOGHI { get; set; }
    }
}
