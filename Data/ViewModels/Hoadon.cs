﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data;

namespace Data.ViewModels
{
    public class Hoadon
    {
        public int IDKHACHHANG { get; set; }
        public string HOTEN { get; set; }
        public string SONHA { get; set;}
        public string DANHSO {get; set;}
        public string MASOCD { get; set; }
        public string MASOGHI { get; set; }
        public string TENSOGHI { get; set; }
        public string MAKHUVUC { get; set; }
        public string TENKHUVUC { get; set; }
        public string MAHTTT { get; set; }
        public string TENHTTT { get; set; }
        public string MADACDIEM { get; set; }
        public string TENDACDIEM { get; set; }
        public decimal TIENVS { get; set; }
        public decimal THUE { get; set; }
        public decimal THANHTIENVS { get; set; }
        public int IDDUONG { get; set; }
        public string TENDUONG { get; set; }
        public bool THANHLYHD { get; set; }
        public string NHANVIENTHU { get; set; }
        public string MASOTHUE { get; set; }
        public string CHUKYNO { get; set; }
        public string SOTK { get; set; }
        public string TRANGTHAI { get; set; }
        public string GHICHU { get; set; }
        public string KEY { get; set; }
        public int IDBGVS { get; set; }
        public int? SO_HOA_DON { get; set; }
        public Hoadon()
        { }
        public Hoadon(HOADON hOADON)
        {
            this.CHUKYNO = hOADON.CHUKYNO;
            this.DANHSO= hOADON.DANHSO;
            this.GHICHU = hOADON.GHICHU;
            this.HOTEN = hOADON.HOTEN;
            this.IDBGVS = (int)hOADON.IDBGVS;
            this.IDDUONG = (int)hOADON.IDDUONG;
            this.IDKHACHHANG = hOADON.IDKHACHHANG;
            this.MADACDIEM = hOADON.MADACDIEM;
            this.MAHTTT = hOADON.MAHTTT;
            this.MAKHUVUC = hOADON.MAKHUVUC;
            this.MASOCD = hOADON.MASOCD;
            this.MASOGHI = hOADON.MASOGHI;
            this.MASOTHUE = hOADON.MASOTHUE;
            this.NHANVIENTHU = hOADON.NHANVIENTHU;
            this.SONHA = hOADON.SONHA;
            this.SOTK = hOADON.SOTK;
            this.THANHLYHD = (bool)hOADON.THANHLYHD;
            this.THANHTIENVS = (decimal)hOADON.THANHTIENVS;
            this.THUE = (decimal)hOADON.THUE;
            this.TIENVS = (decimal)hOADON.TIENVS;
            this.TRANGTHAI = hOADON.TRANGTHAI;
        }
    }

}