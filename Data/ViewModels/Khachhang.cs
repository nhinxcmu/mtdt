﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data;

namespace Data.ViewModels
{
    public class Khachhang
    {
        public int IDKHACHHANG { get; set; }
        public string HOTEN { get; set; }
        public string SONHA { get; set; }
        public string DANHSO { get; set; }
        public string MASOCD { get; set; }
        public string MASOGHI { get; set; }
        public string TENSOGHI { get; set; }
        public string MAKHUVUC { get; set; }
        public string TENKHUVUC { get; set; }
        public string MAHTTT { get; set; }
        public string TENHTTT { get; set; }
        public string MADACDIEM { get; set; }
        public string TENDACDIEM { get; set; }
        public decimal TIENVS { get; set; }
        public decimal THUE { get; set; }
        public decimal THANHTIENVS { get; set; }
        public int IDDUONG { get; set; }
        public string TENDUONG { get; set; }
        public bool THANHLYHD { get; set; }
        public string NHANVIENTHU { get; set; }
        public string MASOTHUE { get; set; }
        public string SOTK { get; set; }
        public string TRANGTHAI { get; set; }
        public int IDBGVS { get; set; }
        public string GHICHU { get; set; }
        public string SDT { get; set; }
        public string EMAIL { get; set; }
        public string DIA_CHI { get; set; }
        public int? STT { get; set; }

    }
    public class Soghi
    {
        public IEnumerable<SOGHI> SOGHIs { get; set; }
    }

}