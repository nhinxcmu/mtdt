﻿using Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data
{
    public partial class HOADON
    {
    }
    public class InvoiceGetList
    {
        public string MASOCD { get; set; }
        public string Key { get; set; }
        public int CHUKYNO { get; set; }
        public string TrangThai { get; set; }
        public string InvoiceName { get; set; }
        public string InvoicePattern { get; set; }
        public string SerialNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CusCode { get; set; }
        public string CusName { get; set; }
        public string CusAddress { get; set; }
        public string CusPhone { get; set; }
        public string CusTaxCode { get; set; }
        public string CusBankNo { get; set; }
        public string PaymentMethod { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> VATRate { get; set; }
        public Nullable<decimal> VATAmount { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string AmountInwords { get; set; }
        public Nullable<System.DateTime> SignDate { get; set; }
        public string KindOfService { get; set; }
        public Nullable<System.DateTime> ArisingDate { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public Nullable<System.DateTime> NGAYUPDATE { get; set; }
        public int? IDDUONG { get; set; }
        public string MASOGHI { get; set; }
        public string HOTEN { get; set; }
        public bool PHAT_HANH_CHUNG { get; set; }

    }
    public class ChuKy
    {
        public string THANG { get; set; }
        public string CHUKYNO { get; set; }
    }
}