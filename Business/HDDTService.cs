﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;
using System.Web.UI;

namespace Business
{
    public class HDDTService
    {
        public async Task<string> UpdateCus(string xmlCusData, string username, string pass, int convert)
        {
            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=UpdateCus";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"xmlCusData",xmlCusData},
                {"username",username},
                {"pass",pass},
                {"convert",convert.ToString()},
            };
            var result = await Post(url, data);
            return result;
        }
        public async Task<string> replaceInv(string Account, string ACpass, string xmlInvData, string username, string password, string fkey, int convert)
        {
            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=replaceInv";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"Account",Account},
                {"ACpass",ACpass},
                {"xmlInvData",xmlInvData},
                {"username",username},
                {"pass",password},
                {"fkey",fkey},
                {"convert",convert.ToString()},
            };
            var result = await Post(url, data);
            return result;
        }
        public async Task<string> cancelInv(string Account, string ACpass, string username, string password, string fkey)
        {


            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=cancelInv";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"Account",Account},
                {"ACpass",ACpass},
                {"fkey",fkey},
                {"username",username},
                {"password",password},
            };
            var result = await Post(url, data);
            return result;
        }

        public async Task<string> ImportAndPublishInv(string Account, string ACpass, string xmlInvData, string username, string pass, string pattern, string serial, int convert)
        {
            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=ImportAndPublishInv";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"Account",Account},
                {"ACpass",ACpass},
                {"xmlInvData",xmlInvData},
                {"username",username},
                {"pass",pass},
                {"pattern",pattern},
                {"serial",serial},
                {"convert",convert.ToString()},
            };
            var result = await Post(url, data);
            return result;
        }

        public async Task<string> confirmPaymentFkey(string lstFkey, string userName, string userPass)
        {
            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=confirmPaymentFkey";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"lstFkey",lstFkey},
                {"userName",userName},
                {"userPass",userPass},
            };
            var result = await Post(url, data);
            return result;
        }
        public async Task<string> UnconfirmPaymentFkey(string lstFkey, string userName, string userPass)
        {
            var url = "https://capnuoccamauservicedemo.vnpt-invoice.com.vn/PublishService.asmx?op=UnconfirmPaymentFkey";
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"lstFkey",lstFkey},
                {"userName",userName},
                {"userPass",userPass},
            };
            var result = await Post(url, data);
            return result;
        }

        async Task<string> Post(string url, Dictionary<string, string> data)
        {
            HttpClient client = new HttpClient();
            if (data.Count > 0)
            {
                url += "?";
                foreach (var para in data)
                {
                    url += para.Key + "=" + para.Value + "&";
                }
            }
            try
            {
                var response = await client.PostAsync(url, null);
                if (response.IsSuccessStatusCode)
                {
                    var responseString = response.Content.ReadAsStringAsync();
                    return responseString.Result.ToString();
                }
                else
                    return "";
            }
            catch (HttpRequestException ex)
            {

                return "-1";
            }





        }
    }
}
