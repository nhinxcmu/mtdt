﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data.SqlClient;
using Data;

namespace Business
{    
    public class KHACHHANG
    {        
        public static IQueryable Index ()
        {
            Data.MTDTEntities db = new Data.MTDTEntities();            
            var lst_ck = db.CHUKY_KHUVUC.GroupBy(x => x.chuky).Select(x => x.FirstOrDefault());
            return lst_ck;
        }


        public static IEnumerable<Data.ViewModels.Khachhang> Khachhang (int? chuky)
        {
            Data.MTDTEntities db = new Data.MTDTEntities();
            var conn = Business.Connection.ConnectionString();
            conn.Open();
            SqlCommand cmd = new SqlCommand("select distinct chuky from chuky_khuvuc where chuky ='" + chuky + "'", conn);
            var result = cmd.ExecuteScalar();
            /*if (result == DBNull.Value)
                return HttpNotFound();*/
            SqlCommand delck = new SqlCommand("delete from khachhang", conn);
            delck.ExecuteNonQuery();
            SqlCommand cmd_kh2 = new SqlCommand("update a set a.tienvs = b.tienvs, a.thue = b.thue, a.thanhtienvs = b.thanhtienvs from Khachhang_" +chuky + " a, BANGGIAVESINH b where a.idbgvs = b.idbgvs and b.idbgvs != 5", conn);
            cmd_kh2.ExecuteNonQuery();
            SqlCommand ck = new SqlCommand("insert into khachhang select * from khachhang_" + chuky + "", conn);
            ck.ExecuteNonQuery();
            IEnumerable<Data.ViewModels.Khachhang> KH = null;
            KH = (from a in db.KHACHHANGs
                  join b in db.SOGHIs on a.MASOGHI equals b.MASOGHI
                  join c in db.DACDIEMs on a.MADACDIEM equals c.MADACDIEM
                  join d in db.DUONGs on a.IDDUONG equals d.IDDUONG
                  join e in db.HINHTHUCTTs on a.MAHTTT equals e.MAHTTT into sr
                  from x in sr.DefaultIfEmpty()
                  orderby a.DANHSO
                  select new Data.ViewModels.Khachhang
                  {
                      IDKHACHHANG = a.IDKHACHHANG,
                      HOTEN = a.HOTEN,
                      SONHA = a.SONHA,
                      DANHSO = a.DANHSO,
                      MASOCD = a.MASOCD,
                      TIENVS = (decimal)a.TIENVS,
                      THUE = (decimal)a.THUE,
                      THANHTIENVS = (decimal)a.THANHTIENVS,
                      TENSOGHI = b.TENSOGHI,
                      TENDACDIEM = c.TENDACDIEM,
                      TENDUONG = d.TENDUONG,
                      MASOGHI = a.MASOGHI,
                      IDDUONG = d.IDDUONG,
                      MAKHUVUC = a.MAKHUVUC,
                      MAHTTT = x.MAHTTT == null ? "": x.MAHTTT,
                      TENHTTT = x.TENHTTT == null ? "" : x.TENHTTT,
                      THANHLYHD = (bool)a.THANHLYHD == null ? false: (bool)a.THANHLYHD,
                      NHANVIENTHU = a.NHANVIENTHU == null ? "" : a.NHANVIENTHU,
                      MASOTHUE = a.MASOTHUE,
                      TRANGTHAI = a.TRANGTHAI,
                      SOTK = a.SOTK,
                      IDBGVS = (int)a.IDBGVS,
                      GHICHU = a.GHICHU
                  }
                );
            return KH;
        }
        
    }
}
