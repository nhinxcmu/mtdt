﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Data;

namespace Business
{    
    public class NGUOIDUNG
    {        
        public static int Login (string tendangnhap, string matkhau)
        {
            Data.MTDTEntities db = new Data.MTDTEntities();
            int r;
            var upassword = GetMD5(tendangnhap+matkhau);
            bool userValid = db.NGUOIDUNGs.Any(s => s.TENDANGNHAP == tendangnhap && s.MATKHAU == upassword && s.TRANGTHAI == true);
            if (userValid)
            {
                r = 1;
            }                
            else 
                r = 0;            
            return r;
        }        
        public static string GetMD5(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            byte[] bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(str));

            StringBuilder sbHash = new StringBuilder();

            foreach (byte b in bHash)
            {
                sbHash.Append(String.Format("{0:x2}", b));
            }
            return sbHash.ToString();
        }
    }
}
