﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;

namespace Business
{
    public class Connection
    {
        public static string getConnectionString()
        {
            //string sqlConnectionString = @"Data Source=10.96.37.194;Initial Catalog=MTDT;User Id=sa;Password=Cmu#2015;Persist Security Info=True;MultipleActiveResultSets=true";//Chuoi ket noi csdl
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["MTDTEntities2"].ConnectionString;
            return sqlConnectionString;
        }
        public static SqlConnection ConnectionString()
        {
            string sqlConnectionString = getConnectionString();//Chuoi ket noi csdl
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            return conn;
        }
        /*public static OleDbConnection ExcelConnectionString(string path1)
        {
            string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";//Chuoi ket noi Oledb
            OleDbConnection excelConnection = new OleDbConnection(excelConnectionString); //Tao ket noi Oledb
            return excelConnection;
        }*/
    }
}
