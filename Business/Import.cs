﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data.SqlClient;
using Data;

namespace Business
{    
    public class IMPORT
    {
        public static string CHUKY(string chukycu)
        {
            string chuky_nam = chukycu.Substring(0, 4);
            string chuky_thang = chukycu.Substring(4);
            string chuky_nam_moi = "";
            string chuky_thang_moi = "";
            string chukymoi = "";
            if (chuky_thang == "12")
            {
                chuky_nam_moi = Convert.ToString(Convert.ToInt32(chuky_nam) + 1);
                chuky_thang_moi = "01";
            }
            else
            {
                chuky_nam_moi = chuky_nam;
                chuky_thang_moi = Convert.ToString(Convert.ToInt32(chuky_thang) + 1);
                switch(chuky_thang_moi)
                {
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        chuky_thang_moi = "0" + chuky_thang_moi;
                        break;
                }
            }
            chukymoi = chuky_nam_moi + chuky_thang_moi;
            return chukymoi;
        }
        
    }
}
