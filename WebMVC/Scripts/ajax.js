﻿function nam_change(nam) {
    $.ajax({
        type: "POST",
        url: '@Url.Action("getChukybyNam","Report")',
        dataType: "json",
        data: { nam: nam },
        success: function (data) {
            $("#chuky").html("");
            $("#khuvuc").html("");
            $("#chuky").append('<option value="0">Chọn tháng</option>');
            $("#khuvuc").append('<option value="0">Chọn khu vực</option>');
            $.each(data, function (i, data2) {
                $("#chuky").append('<option value= "' + data2.CHUKY + '">' + data2.TENCHUKY + '</option>');
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

function chuky_change(chuky) {
    $.ajax({
        type: "POST",
        url: '@Url.Action("getKhuvuc","Report")',
        dataType: "json",
        data: { chuky: chuky },
        success: function (data) {
            $("#khuvuc").html("");
            $("#khuvuc").append('<option value="0">Chọn khu vực</option>');
            $.each(data, function (i, data2) {
                $("#khuvuc").append('<option value= "' + data2.MAKHUVUC + '">' + data2.TENKHUVUC + '</option>');
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}
