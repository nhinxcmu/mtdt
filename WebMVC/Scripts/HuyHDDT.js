﻿$(document).ready(function () {
    function getChuKyByNam() {
        $("#chuKyNo").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetChuKyByNam',
            dataType: "json",
            data: { nam: $('#txtNam').val() },
            success: function (data) {
                data.forEach(function (chuKy) {
                    $("#chuKyNo").append('<option value= "' + chuKy.CHUKYNO + '">' + chuKy.THANG + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    function getSerialNo() {
        $("#serialNo").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetSerialNoByChuKy',
            dataType: "json",
            data: { chuKy: $('#chuKyNo').val() },
            success: function (data) {
                data.forEach(function (serialNo) {
                    $("#serialNo").append('<option value= "' + serialNo + '">' + serialNo + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    function getInvoicePatternByChuKy() {
        $("#invoicePattern").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetInvoicePatternByChuKy',
            dataType: "json",
            data: { chuKy: $('#chuKyNo').val() },
            success: function (data) {
                $("#invoicePattern").append('<option value= "0">Tất cả</option>');
                data.forEach(function (invoicePattern) {
                    $("#invoicePattern").append('<option value= "' + invoicePattern + '">' + invoicePattern + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    function getKhuVucByChuKy() {
        $("#khuvuc").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetKhuVucByChuKy',
            dataType: "json",
            data: { chuKy: $('#chuKyNo').val() },
            success: function (data) {
                $("#khuvuc").append('<option value= "0">Tất cả</option>');
                data.forEach(function (khuVuc) {
                    $("#khuvuc").append('<option value= "' + khuVuc.MAKHUVUC + '">' + khuVuc.TENKHUVUC + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    function getDuongByKhuVuc() {
        $("#duong").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetDuongByKhuVuc',
            dataType: "json",
            data: { khuVuc: $('#khuvuc').val() },
            success: function (data) {
                $("#duong").append('<option value= "0">Tất cả</option>');
                data.forEach(function (duong) {
                    $("#duong").append('<option value= "' + duong.IDDUONG + '">' + duong.TENDUONG + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    function getSoGhiByDuong() {
        $("#soghi").html('');
        $.ajax({
            type: "POST",
            url: '/HDDT/GetSoGhiByDuong',
            dataType: "json",
            data: { duong: $('#duong').val() },
            success: function (data) {
                $("#soghi").append('<option value= "0">Tất cả</option>');
                data.forEach(function (soGhi) {
                    $("#soghi").append('<option value= "' + soGhi.MASOGHI + '">' + soGhi.TENSOGHI + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
    $('#txtNam').change(function () {
        getChuKyByNam();
        getSerialNo();
    });
    $('#chuKyNo').change(function () {
        getSerialNo();
        getInvoicePatternByChuKy();
        getKhuVucByChuKy();
        getDuongByKhuVuc();
        getSoGhiByDuong();
    });
    $('#khuvuc').change(function () {
        getDuongByKhuVuc();
    });
    $('#duong').change(function () {
        getSoGhiByDuong();
    });
    $('#txtNam').val(new Date().getFullYear());
    getChuKyByNam();
    getSerialNo();
    getInvoicePatternByChuKy();
    getKhuVucByChuKy();
    getDuongByKhuVuc();
    getSoGhiByDuong();

    $('#frmHuy').ajaxForm({
        beforeSend: function () {
            $('.nhi-fade').css('display', 'block');
        },
        uploadProgress: function (event, position, total, percentComplete) {

        },
        success: function (data) {
            if (!data.includes("ERR")) {
                alert("Hủy thành công " + data + " hóa đơn.");
                window.location.href = "/HDDT/Huy";
            }
        },
        complete: function (response) {

            $('.nhi-fade').css('display', 'none');
        },
        error: function () {

        }
    });

});