﻿$(document).ready(function () {
    var hoaDonUrl = "/Hoadon/";
    var hdtdUrl = "/HDDT/"
    function getChuky(nam) {
        $.ajax({
            type: "POST",
            url: hoaDonUrl + 'getChukybyNam',
            dataType: "json",
            data: { nam: nam },
            success: function (data) {
                $("#chuky").html("");
                $("#khuvuc").html("");
                $("#duong").html("");
                $("#soghi").html("");
                $("#chuky").append('<option value="0">Chọn tháng</option>');
                $("#khuvuc").append('<option value="0">Chọn khu vực</option>');
                $("#duong").append('<option value="0">Chọn đường</option>');
                $("#soghi").append('<option value="0">Chọn sổ ghi</option>');
                $.each(data, function (i, data2) {
                    $("#chuky").append('<option value= "' + data2.CHUKY + '">' + data2.TENCHUKY + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    var nam = $("#nam").val().toString();
    getChuky(nam);


    $("#nam").change(function () {
        var nam = $("#nam").val().toString();
        getChuky(nam);
    });
    $("#chuky").change(function () {
        var chuky = $("#chuky").val().toString();
        $.ajax({
            type: "POST",
            url: hoaDonUrl + 'getKhuvuc',
            dataType: "json",
            data: { chuky: chuky },
            success: function (data) {
                $("#khuvuc").html("");
                $("#duong").html("");
                $("#soghi").html("");
                $("#khuvuc").append('<option value="0">Chọn khu vực</option>');
                $("#khuvuc").append('<option value="0">Tất cả</option>');
                $("#duong").append('<option value="0">Chọn đường</option>');
                $("#soghi").append('<option value="0">Chọn sổ ghi</option>');
                $.each(data, function (i, data2) {
                    $("#khuvuc").append('<option value= "' + data2.MAKHUVUC + '">' + data2.TENKHUVUC + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    $("#khuvuc").change(function () {
        var khuvuc = $("#khuvuc").val().toString();
        $.ajax({
            type: "POST",
            url: hoaDonUrl + 'getDuong',
            dataType: "json",
            data: { khuvuc: khuvuc },
            success: function (data) {
                $("#duong").html("");
                $("#soghi").html("");
                $("#duong").append('<option value="0">Chọn đường</option>');
                $("#duong").append('<option value="0">Tất cả</option>');
                $("#soghi").append('<option value="0">Chọn sổ ghi</option>');
                $.each(data, function (i, data2) {
                    $("#duong").append('<option value= "' + data2.IDDUONG + '">' + data2.TENDUONG + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    $("#duong").change(function () {
        var duong = $("#duong").val().toString();
        $.ajax({
            type: "POST",
            url: hoaDonUrl + 'getSoGhi',
            dataType: "json",
            data: { duong: duong },
            success: function (data) {
                $("#soghi").html("");
                $("#soghi").append('<option value="0">Tất cả</option>');
                $.each(data, function (i, data2) {
                    $("#soghi").append('<option value= "' + data2.MASOGHI + '">' + data2.TENSOGHI + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    $('#txtNam').change(function () {
        $.ajax({
            type: "POST",
            url: hoaDonUrl + 'getChuKybyNam',
            dataType: "json",
            data: { nam: $('#txtNam').val() },
            success: function (data) {
                $("#sltThang").html('');
                $.each(data, function (i, data2) {
                    $("#sltThang").append('<option value= "' + data2.CHUKY + '">' + data2.TENCHUKY + '</option>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    $('#searchForm').submit(function () {
        $.ajax({
            type: "POST",
            url: hdtdUrl + 'GetInvoices',
            dataType: "json",
            data: {
                maSoCD: $('#txtMaSoCD').val(),
                chuKyNo: $('#sltThang').val(),
            },
            success: function (data) {
                $("#tblInvoices tbody").html('');
                $.each(data, function (i, data2) {
                    $("#tblInvoices tbody").append('<tr> <td>' + data2.Key + '</td> <td>' + data2.CusName + '</td> <td>' + new Date(data2.ArisingDate).toLocaleDateString('vi-vi') + '</td> <td>' + data2.SerialNo + '</td> <td>' + data2.InvoicePattern + '</td> <td>' + data2.Status + '</td></tr>');
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
        return false;
    });
    $('#frmPhatHanh').ajaxForm({
        beforeSend: function () {
            $('.nhi-fade').css('display', 'block');
        },
        uploadProgress: function (event, position, total, percentComplete) {

        },
        success: function (data) {
            if (!data.includes("ERR")) {
                alert("Phát hành thành công " + data + " hóa đơn.");
                window.location.href = hdtdUrl + 'PhatHanh';
            }
        },
        complete: function (response) {

            $('.nhi-fade').css('display', 'none');
        },
        error: function () {

        }
    });
    
});