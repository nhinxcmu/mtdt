﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebMVC.InvoiceServices
{
    public class ImportAndPublishInv
    {
        public string key { get; set; }
        public Invoice Invoice { get; set; }
    }
    public class xmlInvDataImportAndPublish
    {
        public xmlInvDataImportAndPublish()
        {
            Invoices = new List<ImportAndPublishInv>();
        }
        public List<ImportAndPublishInv> Invoices { get; set; }
        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(Invoices.GetType());
            serializer.Serialize(stringwriter, Invoices);
            var xml = stringwriter.ToString().Replace("ArrayOfImportAndPublishInv","Invoices");
            xml=xml.Replace("ImportAndPublishInv", "Inv");
            return xml;
        }
    }
}
