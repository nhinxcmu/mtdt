﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebMVC.InvoiceServices
{
    public class ReplaceInv:Invoice
    {
        public string key { get; set; }
    }
    public class xmlInvDataReplace
    {
        public ReplaceInv ReplaceInv { get; set; }
        public xmlInvDataReplace()
        {
            ReplaceInv = new ReplaceInv();
        }
        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(ReplaceInv.GetType());
            serializer.Serialize(stringwriter, ReplaceInv);
            var xml = stringwriter.ToString();
            return xml;
        }

    }
}
