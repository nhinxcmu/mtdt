﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Data;
namespace WebMVC.InvoiceServices
{
    public class Invoice
    {
        public string CusCode { get; set; }
        public string CusName { get; set; }
        public string Buyer { get; set; }
        public string CusBankNo { get; set; }
        public string CusAddress { get; set; }
        public string CusPhone { get; set; }
        public string CusTaxCode { get; set; }
        public string PaymentMethod { get; set; }
        public string KindOfService { get; set; }
        public List<Product> Products { get; set; }
        public string Total { get; set; }
        public string DiscountAmount { get; set; }
        public string VATRate { get; set; }
        public string VATAmount { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string Extra { get; set; }
        public string ArisingDate { get; set; }
        public string PaymentStatus { get; set; }
        public string ResourceCode { get; set; }
        public Invoice()
        {
            Products = new List<Product>();
        }
        public Invoice(INVOICE invoice,List<Product> products)
        {
            Products = new List<Product>();
            this.Amount = invoice.Amount==null?"0": invoice.Amount.ToString();
            this.AmountInWords = invoice.AmountInwords.ToString();
            this.ArisingDate = invoice.ArisingDate.Value.ToString("dd/MM/yyyy");
            this.CusAddress = invoice.CusAddress==null?"": invoice.CusAddress.ToString();
            this.CusBankNo = invoice.CusBankNo;
            this.CusCode = invoice.CusCode == null ? "" : invoice.CusCode.ToString();
            this.CusName = invoice.CusName.ToString();
            this.CusPhone = invoice.CusPhone == null ? "" : invoice.CusPhone.ToString();
            this.CusTaxCode = invoice.CusTaxCode == null ? "" : invoice.CusTaxCode.ToString();
            this.DiscountAmount = invoice.DiscountAmount == null ? "0" : invoice.DiscountAmount.ToString();
            //this.Extra = invoice.Extra.ToString();
            this.KindOfService = invoice.KindOfService.ToString();
            this.PaymentMethod = invoice.PaymentMethod.ToString();
            this.PaymentStatus = invoice.PaymentStatus.ToString();
            this.Products = products;
            //this.ResourceCode = products;
            this.Total = invoice.Total.ToString();
            this.VATAmount = invoice.VATAmount.ToString();
            this.VATRate = invoice.VATRate.ToString();
            this.Buyer = invoice.Buyer;
        }
    }

    public class xmlInvData
    {
        public List<Invoice> Invoices { get; set; }
        public xmlInvData()
        {
            Invoices = new List<Invoice>();
        }
        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(Invoices.GetType());
            serializer.Serialize(stringwriter, Invoices);
            var xml = stringwriter.ToString().Replace("ArrayOfInvoice", "Invoices");
            return xml;
        }
    }
    

}
