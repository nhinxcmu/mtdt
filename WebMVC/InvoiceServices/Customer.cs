﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebMVC.InvoiceServices
{
    public class Customer
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string TaxCode { get; set; }
        public string Address { get; set; }
        public string BankAccountName { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string ContactPerson { get; set; }
        public string RepresentPerson { get; set; }
        public string CusType { get; set; }
        public Customer(CUSTOMER customer)
        {
            Name = customer.Name;
            Code = customer.Code;
            TaxCode = customer.TaxCode;
            Address = customer.Address;
            BankAccountName = customer.BankAccountName;
            BankName = customer.BankName;
            BankNumber = customer.BankNumber;
            Email = customer.Email;
            Fax = customer.Fax;
            Phone = customer.Phone;
            ContactPerson = customer.ContactPerson;
            RepresentPerson = customer.RepresentPerson;
            CusType = customer.CusType;
        }
        public Customer()
        { }
    }
    public class xmlCusDataUpdate
    {
        public ICollection<Customer> Customers { get; set; }
        public xmlCusDataUpdate()
        {
            Customers = new List<Customer>();
        }

        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(Customers.GetType());
            serializer.Serialize(stringwriter,Customers);
            var xml = stringwriter.ToString().Replace("ArrayOfCustomer", "Customers");
            return xml;
        }
    }
}
