﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.InvoiceServices
{
    public class Item
    {
        public string fkey { get; set; }
        public string index { get; set; }
        public string cusCode { get; set; }
        public string name { get; set; }
        public string publishDate { get; set; }
        public string signStatus { get; set; }
        public string pattern { get; set; }
        public string serial { get; set; }
        public string invNum { get; set; }
        public string payment { get; set; }
        public string amount { get; set; }
        public string status { get; set; }
    }
}
