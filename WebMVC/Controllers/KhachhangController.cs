﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;
using Data.ViewModels;
using WebMVC.InvoiceServices;

namespace WebMVC.Controllers
{
    public class KhachhangController : Controller
    {
        private MTDTEntities db = new MTDTEntities();

        [Authorize]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Home");
            //ViewBag.ChukyList = new SelectList(db.GetChuky(), "CHUKY", "TENCHUKY");
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View(Business.KHACHHANG.Index());
        }
        //Xu ly Ajax dropdownlist
        public JsonResult getChukybyNam(string nam)
        {
            var result = db.getChukybyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getKhuvuc(string chuky)
        {
            var result = db.getKhuvucbyChuky(chuky); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDuong(string khuvuc)
        {
            var result = db.getDuongbyKhuvuc(khuvuc, "");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSoghi(int duong)
        {
            var result = db.getSoghibyDuong(duong);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getBanggia(int idbgvs)
        {
            var result = db.getBGVSbyIDBGVS(idbgvs);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getBGVS()
        {
            var result = db.GetBGVS();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getHTTT()
        {
            var result = db.GetHTTT();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDacdiem()
        {
            var result = db.GetDacdiem();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Trang chi tiet KH trong 1 CK theo KV, D, SG
        [Authorize]
        public ActionResult Khachhang(int? chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi)//Danh sach khach hang theo chu ky
        {
            if (chuky.HasValue)
            {
                ViewBag.chuky = chuky;
                ViewBag.khuvuc = khuvuc;
                ViewBag.duong = duong;
                ViewBag.soghi = soghi;
                ViewBag.searchString = searchString==null?"":searchString;
                ViewBag.DuongList = new SelectList(db.getDuongbyKhuvuc(khuvuc, ""), "IDDUONG", "TENDUONG");
                ViewBag.SoghiList = new SelectList(db.getSoghibyKhuvuc(khuvuc, ""), "MASOGHI", "TENSOGHI");
                ViewBag.DacdiemList = new SelectList(db.GetDacdiem(), "MADACDIEM", "TENDACDIEM");
                ViewBag.HTTTList = new SelectList(db.GetHTTT(), "MAHTTT", "TENHTTT");
                ViewBag.BGList = new SelectList(db.GetBGVS(), "IDBGVS", "TENBGVS");


                if (searchString != null)
                { page = 1; }
                else
                { searchString = currentFilter; }
                ViewBag.CurrentFilter = searchString;
                int pageSize = 50;
                if (pagesize.HasValue)
                {
                    pageSize = (int)pagesize;
                }
                ViewBag.pagesize = pageSize;
                int pageNumber = (page ?? 1);
                ViewBag.CurrentPage = page;
                var lst_kh = db.Database.SqlQuery<Khachhang>("GetKhachhang @i_chuky={0}, @i_makhuvuc={1}, @i_idduong={2}, @i_masoghi={3}, @i_searchstring={4}", chuky, khuvuc, duong, soghi, searchString);
                
                
                if (soghi != "0")
                {
                    ViewBag.tensoghi = db.getSoghibyMasoghi(soghi).Select(a => a.TENSOGHI).FirstOrDefault();
                }
                if (khuvuc != "0")
                {
                    ViewBag.tenkhuvuc = db.getKhuvucbyMakhuvuc(khuvuc).Select(a => a.TENKHUVUC).FirstOrDefault();
                }
                if (duong != 0)
                {
                    ViewBag.tenduong = db.getDuongbyIdduong(duong).Select(a => a.TENDUONG).FirstOrDefault();
                }

                return View(lst_kh.ToPagedList(pageNumber, pageSize));
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [Authorize]
        public ActionResult CapnhatKH(int chuky, int idkh)//Trang cap nhat KH
        {
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            //return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            else
            {
                string khuvuc = "";
                int duong = 0;
                int idbgvs = 0;

                var conn = Business.Connection.ConnectionString();
                conn.Open();
                SqlCommand cmd3 = new SqlCommand("exec getKhachhangbyIdkhachhang @i_chuky='" + chuky + "', @i_idkhachhang=" + idkh, conn);
                var reader = cmd3.ExecuteReader();
                while (reader.Read())
                {
                    ViewBag.hoten = reader[0];
                    ViewBag.sonha = reader[1];
                    ViewBag.masothue = reader[2];
                    ViewBag.masocd = reader[3];
                    ViewBag.tensoghi = reader[4];
                    ViewBag.masoghi = reader[5];
                    ViewBag.madacdiem = reader[6];
                    ViewBag.tendacdiem = reader[7];
                    ViewBag.mahttt = reader[8];
                    ViewBag.tenhttt = reader[9];
                    ViewBag.tlhd = reader[10];
                    ViewBag.thanhtienvs = reader[11];
                    ViewBag.makhuvuc = reader[12];
                    ViewBag.idduong = reader[13];
                    ViewBag.idbgvs = reader[14];
                    ViewBag.danhso = reader[15];
                    ViewBag.sotk = reader[16];
                    khuvuc = reader[12].ToString();
                    duong = (int)reader[13];
                    idbgvs = (int)reader[14];
                }


                //else
                //{
                //    return HttpNotFound();
                //}


                ViewBag.DuongList = new SelectList(db.getDuongbyKhuvuc(khuvuc, ""), "IDDUONG", "TENDUONG", ViewBag.idduong);
                ViewBag.SoghiList = new SelectList(db.getSoghibyDuong(duong), "MASOGHI", "TENSOGHI", ViewBag.masoghi);
                ViewBag.DacdiemList = new SelectList(db.GetDacdiem(), "MADACDIEM", "TENDACDIEM");
                ViewBag.HTTTList = new SelectList(db.GetHTTT(), "MAHTTT", "TENHTTT", ViewBag.mahttt);
                ViewBag.BGList = new SelectList(db.GetBGVS(), "IDBGVS", "TENBGVS", ViewBag.idbgvs);
                ViewBag.chuky = chuky;
                ViewBag.idkh = idkh;
                return View();
            }
        }


        //Xu ky cap nhat KH
        [Authorize]
        public JsonResult CapnhatKhachhang(string chuky, string khuvuc, string idkhachhang, string danhso, string masocd, string hoten, string sonha, string msthue, string sotk, string httt, int tlhd, int duong, string soghi, int idbgvs, decimal thanhtienvs, string dacdiem, string ghichu,string cusType,string sdt, string email,string diaChi)
        {
            var result = db.Database.ExecuteSqlCommand("UpdateKhachhang @chuky={0}, @idkhachhang={1}, @hoten ={2}, @masoghi = {3}, @sonha = {4}, @tlhd = {5}, @msthue = {6}, @idduong = {7}, @thanhtienvs={8}, @idbgvs={9}, @danhso={10}, @mscd={11}, @mahttt={12}, @sotk={13}, @madacdiem={14}, @ghichu={15}", chuky, idkhachhang, hoten, soghi, sonha, tlhd, msthue, duong, thanhtienvs, idbgvs, danhso, masocd, httt, sotk, dacdiem, ghichu);
            THONGTINKHACHHANG thongTinKhachHang = db.THONGTINKHACHHANGs.SingleOrDefault(n => n.MASOCD == masocd);
            if (thongTinKhachHang == null)
            {
                thongTinKhachHang = new THONGTINKHACHHANG {
                    EMAIL = email,
                    MASOCD = masocd,
                    SDT = sdt,
                    DIA_CHI = diaChi,
                };
                db.THONGTINKHACHHANGs.Add(thongTinKhachHang);
            }
            else
            {
                thongTinKhachHang.EMAIL = email;
                thongTinKhachHang.SDT = sdt;
                thongTinKhachHang.DIA_CHI = diaChi;
                db.Entry(thongTinKhachHang).State = System.Data.Entity.EntityState.Modified;
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.SingleOrDefault(n => n.Code == danhso);
            if (cUSTOMER != null)
            {
                cUSTOMER.Address = sonha + ", " + db.DUONGs.Find(duong).TENDUONG;
                cUSTOMER.BankAccountName = "";
                cUSTOMER.BankName = "";
                cUSTOMER.BankNumber = "";
                cUSTOMER.Code = danhso;
                cUSTOMER.ContactPerson = "";
                cUSTOMER.CusType = cusType == null ? "0" : cusType;
                cUSTOMER.Email = thongTinKhachHang.EMAIL;
                cUSTOMER.Fax = "";
                cUSTOMER.Name = hoten;
                cUSTOMER.Phone = thongTinKhachHang.SDT;
                cUSTOMER.TaxCode = msthue;
                db.Entry(cUSTOMER).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                cUSTOMER = new CUSTOMER();
                cUSTOMER.Address = sonha + ", " + db.DUONGs.Find(duong).TENDUONG;
                cUSTOMER.BankAccountName = "";
                cUSTOMER.BankName = "";
                cUSTOMER.BankNumber = "";
                cUSTOMER.Code = danhso;
                cUSTOMER.ContactPerson = "";
                cUSTOMER.CusType = cusType == null ? "0" : cusType;
                cUSTOMER.Email = thongTinKhachHang.EMAIL;
                cUSTOMER.Fax = "";
                cUSTOMER.Name = hoten;
                cUSTOMER.Phone = thongTinKhachHang.SDT;
                cUSTOMER.TaxCode = msthue;
                db.CUSTOMERs.Add(cUSTOMER);
            }
            db.SaveChanges();
            var customers = new List<Customer>();
            customers.Add(new Customer(cUSTOMER));
            var updateCusResult = UpdateKhachHang(customers);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //XU ly xoa KH
        [Authorize]
        public JsonResult XoaKhachhang(string chuky, string idkhachhang)
        {
            var result = db.Database.ExecuteSqlCommand("DeleteKhachhang @chuky={0},@idkhachhang={1}", chuky, idkhachhang);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //Xu ly them KH
        [Authorize]
        public JsonResult ThemKhachhang(string chuky, string hoten, string sonha, string mst, int idbgvs, decimal thanhtienvs, string makhuvuc, int idduong, string masoghi, string danhso, string masocd, string sotk, string httt, string dacdiem, string ghichu, string cusType, string sdt, string email,string diaChi)//, string hoten, string sonha, int idbgvs, string thanhtienvs, string masoghi, string mst, int idduong, string makhuvuc)
        {
            //var result = db.Database.ExecuteSqlCommand("InsertKhachhang @chuky={0},@hoten={1},@sonha={2},@idbgvs={3},@thanhtienvs={4},@masoghi={5}, @mst={6}, @idduong={7}, @makhuvuc={8}, @ds={9}, @mscd={10}, @sotk={11}, @httt={12}, @madacdiem={13}, @ghichu={14}", chuky, hoten, sonha, idbgvs, thanhtienvs, masoghi, mst, idduong, makhuvuc, danhso, masocd, sotk, httt, dacdiem, ghichu);
            List<InsertKhachHangResult> result = db.Database.SqlQuery<InsertKhachHangResult>("InsertKhachhang @chuky={0},@hoten={1},@sonha={2},@idbgvs={3},@thanhtienvs={4},@masoghi={5}, @mst={6}, @idduong={7}, @makhuvuc={8}, @ds={9}, @mscd={10}, @sotk={11}, @httt={12}, @madacdiem={13}, @ghichu={14},@sdt={15},@email={16},@diaChi={17}", chuky, hoten, sonha, idbgvs, thanhtienvs, masoghi, mst, idduong, makhuvuc, danhso, masocd, sotk, httt, dacdiem, ghichu,sdt,email,diaChi).ToList();
            //THONGTINKHACHHANG thongTinKhachHang = db.THONGTINKHACHHANGs.SingleOrDefault(n => n.MASOCD == masocd);
            //if (thongTinKhachHang == null)
            //{
            //    thongTinKhachHang = new THONGTINKHACHHANG
            //    {
            //        EMAIL = email,
            //        MASOCD = masocd,
            //        SDT = sdt,
            //        DIA_CHI=diaChi
            //    };
            //    db.THONGTINKHACHHANGs.Add(thongTinKhachHang);
            //}
            //else
            //{
            //    thongTinKhachHang.EMAIL = email;
            //    thongTinKhachHang.SDT = sdt;
            //    thongTinKhachHang.DIA_CHI = diaChi;
            //    db.Entry(thongTinKhachHang).State = System.Data.Entity.EntityState.Modified;
            //}
            //CUSTOMER cUSTOMER = new CUSTOMER
            //{
            //    Address = sonha + ", " + db.DUONGs.Find(idduong).TENDUONG,
            //    BankAccountName = "",
            //    BankName = "",
            //    BankNumber = "",
            //    Code = danhso,
            //    ContactPerson = "",
            //    CusType = cusType == null ? "0" : cusType,
            //    Email = thongTinKhachHang.EMAIL,
            //    Fax = "",
            //    Name = hoten,
            //    Phone = thongTinKhachHang.SDT,
            //    TaxCode = mst,
            //};
            //db.CUSTOMERs.Add(cUSTOMER);
            //var customers = new List<Customer>();
            //customers.Add(new Customer(cUSTOMER));
            //db.SaveChanges();
            //var updateCusResult = UpdateKhachHang(customers);
            return Json(result, JsonRequestBehavior.AllowGet);
            /*if (!User.IsInRole("Administrator"))
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);*/
        }


        //XU ly chuyen danh so KH
        [Authorize]
        public JsonResult chuyenDanhso(string chuky, string khuvuc, string duong, string soghi, int idkhachhang, string danhso)//Xu ky chuyen danh so KH
        {
            var result = 0;
            if (danhso != null && danhso != "")
            {
                result = db.Database.ExecuteSqlCommand("Chuyendanhso @i_idkhachhang={0},@i_danhso={1},@i_chuky={2}", idkhachhang, danhso, chuky);
            }
            //db.Database.ExecuteSqlCommand("DeleteKhachhang @chuky={0},@idkhachhang={1}", id, idkh);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Export(string chuky, string khuvuc, int duong, string soghi)
        {

            GridView gv = new GridView();
            gv.DataSource = db.ExportKhachhang(chuky, khuvuc, duong, soghi).ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ExportKhachhang.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "Utf-8";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            htw.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            gv.RenderControl(htw);
            //string style = @"<style> TD { mso-number-format:\@; } </style>";//Set format cell ve Text
            //Response.Write(style);//
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index");
        }
        #region HDDT
        public int UpdateKhachHang(List<Customer> customers)
        {
            var data = new xmlCusDataUpdate();
            data.Customers = customers;
            var Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            var Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            SERVICE_CALL serviceCall = new SERVICE_CALL();
            serviceCall.CALL_TIME = DateTime.Now;
            serviceCall.FUNCTION_NAME = "UpdateCus";
            serviceCall.XML = data.ToXML();
            db.SERVICE_CALL.Add(serviceCall);
            int result = 0;
            db.SaveChanges();
            try
            {
                result = new PublishService.PublishServiceSoapClient().UpdateCus(data.ToXML(), Username, Password, null);
                serviceCall.RESPONSE = result.ToString();
                db.Entry(serviceCall).State = System.Data.Entity.EntityState.Modified;
            }
            catch (Exception e)
            {
                serviceCall.RESPONSE = e.Message + "  " + e.StackTrace;
                db.Entry(serviceCall).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();
            return result;
        }
        [Authorize]
        public JsonResult ImportKhachHangs(int chuky, string khuvuc, int duong, string soghi, string searchString)
        {
            var lst_kh = db.Database.SqlQuery<Khachhang>("GetKhachhang @i_chuky={0}, @i_makhuvuc={1}, @i_idduong={2}, @i_masoghi={3}, @i_searchstring={4}", chuky, khuvuc, duong, soghi, searchString).ToList();
            //--import khách hàng
            var customers = new List<Customer>();
            var dbCustomers = new List<CUSTOMER>();
            foreach (Khachhang khachHang in lst_kh)
            {
                CUSTOMER customer = db.CUSTOMERs.SingleOrDefault(n => n.Code == khachHang.DANHSO);
                if (customer == null)
                {
                    customer = new CUSTOMER();
                    customer.Address = khachHang.DIA_CHI==null? khachHang.SONHA + ", " + khachHang.TENDUONG+ " TP.Cà Mau":khachHang.DIA_CHI;
                    customer.BankAccountName = "";
                    customer.BankName = "";
                    customer.BankNumber = "";
                    customer.Code = khachHang.DANHSO;
                    customer.ContactPerson = "";
                    customer.CusType = "0";
                    customer.Email = khachHang.EMAIL==null?"":khachHang.EMAIL;
                    customer.Fax = "";
                    customer.Name = khachHang.HOTEN;
                    customer.Phone = khachHang.SDT;
                    customer.TaxCode = khachHang.MASOTHUE;
                }
                customers.Add(new Customer(customer));
                dbCustomers.Add(customer);
               
            }

            int updateKhachHang = UpdateKhachHang(customers);
            if(updateKhachHang>0)
            {
                foreach(var customer in customers)
                    db.CUSTOMERs.AddRange(dbCustomers);
            }
            return Json(updateKhachHang, JsonRequestBehavior.AllowGet); 
        }
        #endregion
    }

}
class InsertKhachHangResult
{
    public string MSCD { get; set; }
}