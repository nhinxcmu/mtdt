﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class DuongController : Controller
    {
        //
        // GET: /HTTT/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_kv = db.Database.SqlQuery<KHUVUC>("Exec KhuvucSelect");
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View(lst_kv);
        }
        public ActionResult Duong(string khuvuc, string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            { page = 1; }
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;
            
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.khuvuc = khuvuc;
            ViewBag.tenkhuvuc = db.getKhuvucbyMakhuvuc(khuvuc).Select(a => a.TENKHUVUC).FirstOrDefault();

            var lst_duong = db.Database.SqlQuery<DUONG>("getDuongbyKhuvuc @i_makhuvuc={0}, @i_searchstring={1}", khuvuc, searchString).ToPagedList(pageNumber, pageSize);
            return View(lst_duong);
        }
        public EmptyResult ThemDuong(string makhuvuc, string tenduong)
        {
            db.Database.ExecuteSqlCommand("InsertDuong @makhuvuc={0}, @tenduong={1}", makhuvuc, tenduong);
            return new EmptyResult();
        }
        public EmptyResult UpdateDuong(int idduong, string tenduong)
        {
            db.Database.ExecuteSqlCommand("UpdateDuong @idduong={0}, @tenduong={1}", idduong, tenduong);
            return new EmptyResult();
        }

        public JsonResult XoaDuong(int id)
        {
            var result = db.Database.ExecuteSqlCommand("DeleteDuong @idduong={0}", id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}