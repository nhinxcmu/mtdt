﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Web.Security;
using Data;
using Business;

namespace WebMVC.Controllers
{
    public class HomeController : Controller
    {
        private MTDTEntities db  = new MTDTEntities();
        [Authorize]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Home");
            else
                return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string tdn, string mk)
        {
            var r = Business.NGUOIDUNG.Login(tdn,mk);
            if(r==1)
            {                
                FormsAuthentication.SetAuthCookie(tdn, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "");                
            }
            return View();            
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}