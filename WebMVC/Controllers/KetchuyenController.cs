﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Data;
using Business;


namespace WebMVC.Controllers
{
    public class KetchuyenController : Controller
    {
        private Data.MTDTEntities db = new Data.MTDTEntities();
        //
        // GET: /Ketchuyen/
        public ActionResult Index()
        {
            var lst_kv = db.Database.SqlQuery<KHUVUC>("Exec KhuvucSelect");
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View(lst_kv);
        }
        public ActionResult getChuky(string khuvuc)
        {
            //var result = (from r in db.CHUKY_KHUVUC where r.makhuvuc == khuvuc select r.chuky).Max();
            var result = db.getChukybyMakhuvuc(khuvuc).Select(a => a.chuky).Max();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Ketchuyen(string khuvuc)
        {
            var result = db.Database.ExecuteSqlCommand("Ketchuyen @makhuvuc={0}", khuvuc);
            return Json(result, JsonRequestBehavior.AllowGet);
            //string s = "<script language='javascript' type='text/javascript'>alert('Không có dữ liệu');window.location.href='/../Ketchuyen/Index'</script>";
            ////Ket noi
            //var conn = Business.Connection.ConnectionString();
            //conn.Open();
            ////Tim max chuky theo tung khuvuc
            //SqlCommand sqlcmd_chuky = new SqlCommand("select max(chuky) from chuky_khuvuc where makhuvuc= '" + khuvuc + "'", conn);            
            //string chukymoi = "";
            //SqlCommand ketchuyen = new SqlCommand("",conn);            
            //if (sqlcmd_chuky.ExecuteScalar() != DBNull.Value)//Neu tim thay max chu ky
            //{
            //    string result_chuky = (string)sqlcmd_chuky.ExecuteScalar();
            //    chukymoi = Business.IMPORT.CHUKY(result_chuky);
            //    //ketchuyen = new SqlCommand("Insert into Khachhang_"+ chukymoi + " select * from khachhang_"+result_chuky+" where makhuvuc = '"+ khuvuc+"'",conn);
            //    //Tao bang moi
            //    string tablenameKH = "KHACHHANG_" + chukymoi;
            //    string tablenameHDT = "HOADONTON_" + chukymoi;
            //    db.Database.ExecuteSqlCommand("CreateTableKhachhang_ketchuyen @tablename={0}", tablenameKH);
            //    db.Database.ExecuteSqlCommand("CreateTableHoadonton_ketchuyen @tablename={0}", tablenameHDT);
            //    //Chuyen du lieu vao bang moi
            //    //ketchuyen.ExecuteNonQuery();
            //    db.Database.ExecuteSqlCommand("Ketchuyen @chukymoi={0},@chukycu={1},@makhuvuc={2}", chukymoi, result_chuky, khuvuc);
            //    //Them thongtin vao chuky_khuvuc
            //    db.Database.ExecuteSqlCommand("InsertCKKV @chuky={0}, @khuvuc={1}", chukymoi, khuvuc);
            //    /*SqlCommand them_ckkv = new SqlCommand("Insert into chuky_khuvuc values ('" + chukymoi + "','" + khuvuc + "')", conn);
            //    them_ckkv.ExecuteNonQuery();*/
            //    s = "<script language='javascript' type='text/javascript'>alert('Kết chuyển thành công sang " + chukymoi + "');window.location.href='~/../Index'</script>";
            //}
            
            //return Content(s);

        }
	}
}