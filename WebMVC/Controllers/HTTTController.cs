﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class HTTTController : Controller
    {
        //
        // GET: /HTTT/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_httt = db.GetHTTT().ToList();
            return View(lst_httt);
        }
        public EmptyResult ThemHTTT(string mahttt, string tenhttt)
        {
            db.Database.ExecuteSqlCommand("InsertHTTT @mahttt={0}, @tenhttt={1}", mahttt, tenhttt);
            return new EmptyResult();
        }
        public EmptyResult UpdateHTTT(string mahttt, string tenhttt)
        {
            db.Database.ExecuteSqlCommand("UpdateHTTT @mahttt={0}, @tenhttt={1}", mahttt, tenhttt);
            return new EmptyResult();
        }
        public EmptyResult XoaHTTT(string id)
        {
            db.Database.ExecuteSqlCommand("DeleteHTTT @mahttt={0}", id);
            return new EmptyResult();
        }
	}
}