﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using Data;
using System.Data.SqlClient;
namespace WebMVC.Controllers
{
    public class AccountController : Controller
    {
        private MTDTEntities db = new MTDTEntities();
        //
        // GET: /Account/
        [Authorize]
        public ActionResult Index()//Them tai khoan moi
        {
            if (!User.IsInRole("Administrator"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View();
        }
        [Authorize]
        public ActionResult InsertNguoidung(string tendangnhap, string hoten, string matkhau, string quyen)
        {
            //var conn = Business.Connection.ConnectionString();
            //conn.Open();
            if (!User.IsInRole("Administrator"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else 
            {
                string matkhau2 = Business.NGUOIDUNG.GetMD5(tendangnhap+matkhau);
                db.Database.ExecuteSqlCommand("InsertNguoidung @tendangnhap = {0}, @matkhau={1}, @quyen={2}, @hoten={3}", tendangnhap, matkhau2, quyen, hoten);
                return new EmptyResult();
                //return Content("<script language='javascript' type='text/javascript'>alert('Thêm thành công');window.location.href='~/../Index'</script>");
            }            
        }

        [Authorize]
        public ActionResult Quyen()
        {
            if (!User.IsInRole("Administrator"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
            {
                 var lst_user = db.NGUOIDUNGs.ToList();
                 return View(lst_user);
            }
        }

        [Authorize]
        public ActionResult UpdateQuyenNguoidung(string tendangnhap, string quyen, string trangthai, string hoten)
        {
            if (User.IsInRole("Administrator"))
            //    return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            //else
            {                
                db.Database.ExecuteSqlCommand("UpdateQuyenNguoidung @tendangnhap = {0}, @quyen={1}, @trangthai={2}, @hoten={3}", tendangnhap, quyen, trangthai, hoten);
                
            }
            return new EmptyResult();
        }

        [Authorize]
        public ActionResult Nguoidung()
        {
            var query = db.NGUOIDUNGs.Where(a => a.TENDANGNHAP == User.Identity.Name).ToList();
            return View(query);
        }
        [Authorize]
        public JsonResult UpdateThongtinNguoidung(string hoten, string matkhau, string tendangnhap)
        {
            string matkhau2 = Business.NGUOIDUNG.GetMD5(tendangnhap+matkhau);
            var result = db.Database.ExecuteSqlCommand("UpdateThongtinNguoidung @tendangnhap = {0}, @matkhau={1}, @hoten={2}", tendangnhap, matkhau2, hoten);
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Content("<script language='javascript' type='text/javascript'>alert('Lưu thành công');window.location.href='~/../Nguoidung'</script>");
        }

	}
}