﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using Business;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Web.UI.WebControls;

namespace WebMVC.Controllers
{
    public class ReportController : Controller
    {
        private MTDTEntities db = new MTDTEntities();
        public JsonResult getChukybyNam(string nam)
        {
            var result = db.getChukybyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getChukyGiaohoadonbyNam(string nam)
        {
            var result = db.getChukyGiaohoadonbyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getKhuvuc(string chuky)
        {
            var result = db.getKhuvucbyChuky(chuky); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLangiaobyMakhuvuc(string chuky, string khuvuc)
        {
            var result = db.getLangiaobyMakhuvuc(chuky, khuvuc); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Report/
        [Authorize]
        public ActionResult Index()
        {            
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
       
        [Authorize]
        public ActionResult Report(string khuvuc, string chuky)
        {
            try
            {
                ReportViewer reportViewer = new ReportViewer();
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportDuong @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                //ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportDuong.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1",dt));                

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4,2);
                var nam = chuky.Substring(0,4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam , false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();                
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                ex.ToString();
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }
        [Authorize]
        public ActionResult Khuvuc()
        {            
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportKhuvuc(string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportKhuvuc @i_chuky = '" + chuky +"'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportKhuvuc.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportKhuvuc_thang", thang, false));
                paramList.Add(new ReportParameter("ReportKhuvuc_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();               
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }
        [Authorize]
        public ActionResult Thang()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportThang(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportThang @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportThang.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();               
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }
        [Authorize]
        public ActionResult Giaohoadon()
        {            
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportGiaohoadon(string chuky, string khuvuc, int langiao)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();                
                string sql = @"exec ReportGiaohoadon @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "', @i_langiao = " + langiao;
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportGiaohoadon.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var lan = langiao.ToString();
                paramList.Add(new ReportParameter("ReportGiaohoadon_thang", thang, false));
                paramList.Add(new ReportParameter("ReportGiaohoadon_nam", nam, false));
                paramList.Add(new ReportParameter("ReportGiaohoadon_lan", lan, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }
        [Authorize]
        public ActionResult ChitietGiaohoadon()
        {
            var lst_ck = db.GIAOHOADONs.GroupBy(x => x.CHUKY).Select(x => x.FirstOrDefault());
            return View(lst_ck.ToList());            
        }
        public ActionResult ReportChitietgiaohoadon(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                db.Database.ExecuteSqlCommand("CreateTabletam @chuky={0}, @khuvuc={1}",chuky,khuvuc);                
                string sql = @"exec ReportChitietgiaohoadon @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportChitietgiaohoadon.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);                
                paramList.Add(new ReportParameter("ReportChitietGiaohoadon_thang", thang, false));
                paramList.Add(new ReportParameter("ReportChitietGiaohoadon_nam", nam, false));               
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                SqlCommand cmd = new SqlCommand("drop table tam", conn);
                cmd.ExecuteNonQuery();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }

        public ActionResult TonghopTondauky()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportTonghopTondauky(string khuvuc, string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportTonghopTondauky @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportTonghopHDTondauky.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }

        public ActionResult TonghopPhatsinh()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportTonghopPhatsinh(string khuvuc, string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportTonghopPhatsinh @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportTonghopHDPhatsinh.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }

        public ActionResult TonghopThuthangtruoc()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportTonghopThuthangtruoc(string khuvuc, string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportTonghopThuthangtruoc @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportTonghopHDThuthangtruoc.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }


        public ActionResult TonghopThuhientai()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportTonghopThuhientai(string khuvuc, string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportTonghopThuhientai @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportTonghopHDThuhientai.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }

        public ActionResult TonghopToncuoiky()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        [Authorize]
        public ActionResult ReportTonghopToncuoiky(string khuvuc, string chuky)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportTonghopToncuoiky_Hoadonton @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này   
                string sql2 = @"exec ReportTonghopToncuoiky_Khachhang @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da2 = new SqlDataAdapter(sql2, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                //DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da2.Fill(dt);//đổ dữ liệu lấy được vào Table dt 
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportTonghopHDToncuoiky.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("ReportDuong_khuvuc", kv, false));
                paramList.Add(new ReportParameter("ReportDuong_thang", thang, false));
                paramList.Add(new ReportParameter("ReportDuong_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();

        }

        [Authorize]
        public ActionResult BangkePhatsinh()
        { 
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportBangkePhatsinh(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangkePhatsinh @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangkePhatsinh.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }
        [Authorize]
        public ActionResult BangkeTondauky()
        {
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportBangkeTondauky(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangkeTondauky @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangkeTondauky.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }
        [Authorize]
        public ActionResult BangkeToncuoiky()
        {
            //var lst_kv = db.Database.SqlQuery<KHUVUC>("Exec KhuvucSelect");
            //return View(lst_kv.ToList());
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportBangkeToncuoiky(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangkeToncuoiky_Hoadonton @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này  
                string sql2 = @"exec ReportBangkeToncuoiky_Khachhang @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da2 = new SqlDataAdapter(sql2, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                //DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da2.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này  
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangkeToncuoiky.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }
        [Authorize]
        public ActionResult BangkeThuthangtruoc()
        { 
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportBangkeThuthangtruoc(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangkeThuthangtruoc @i_chuky = '" + chuky + "', @i_makhuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangkeThuthangtruoc.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }

        [Authorize]
        public ActionResult BangkeThuhientai()
        { 
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View();
        }
        public ActionResult ReportBangkeThuhientai(string chuky, string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangkeThuhientai @i_chuky = '" + chuky + "', @i_makhuvuc = '"+ khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangkeThuhientai.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("ReportThang_thang", thang, false));
                paramList.Add(new ReportParameter("ReportThang_nam", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }
	 }
}