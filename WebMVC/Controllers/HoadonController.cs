﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;
using Data.ViewModels;
using WebMVC.InvoiceServices;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace WebMVC.Controllers
{
    public class HoadonController : Controller
    {
        private MTDTEntities db = new MTDTEntities();
        private PublishService.PublishServiceSoapClient publishServiceClient = new PublishService.PublishServiceSoapClient();
        private BusinessService.BusinessServiceSoapClient businessServiceClient = new BusinessService.BusinessServiceSoapClient();
        private PortalService.PortalServiceSoapClient portalServiceSoapClient = new PortalService.PortalServiceSoapClient();
        string Username = "";
        string Password = "";
        string Account = "";
        string ACpass = "";
        string Pattern = "";
        string Serial = "";
        List<HINHTHUCTT> hinhThucs;
        List<BANGGIAVESINH> services;
        //
        // GET: /Hoadon/
        [Authorize]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Home");
            //var lst_kv = db.Database.SqlQuery<KHUVUC>("Exec KhuvucSelect");
            //ViewBag.KhuvucList = new SelectList(lst_kv, "MAKHUVUC", "TENKHUVUC"); 
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View(Business.KHACHHANG.Index());
        }
        //Xu ly Ajax dropdownlist
        public JsonResult getChukybyNam(string nam)
        {
            var result = db.getChukybyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getKhuvuc(string chuky)
        {
            var result = db.getKhuvucbyChuky(chuky);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDuong(string khuvuc)
        {
            var result = db.getDuongbyKhuvuc(khuvuc, "");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSoghi(int duong)
        {
            var result = db.getSoghibyDuong(duong);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Hoadon(string chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi, string tthoadon, string loaihoadon, string httt)
        {
            if (searchString != null)
            { page = 1; }
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;
            db.Database.ExecuteSqlCommand("InsertHoadon @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}, @tthoadon={4}, @loaihoadon={5}", chuky, khuvuc, duong, soghi, tthoadon, loaihoadon);

            int pageSize = 30;
            if (pagesize.HasValue)
            {
                pageSize = (int)pagesize;
            }
            ViewBag.pagesize = pageSize;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.tthoadon = tthoadon;
            ViewBag.loaihoadon = loaihoadon;
            ViewBag.httt = httt;
            var lst_hd = db.Database.SqlQuery<Hoadon>("Exec GetHoadon @i_searchstring={0}, @i_mahttt={1}", searchString, httt).ToPagedList(pageNumber, pageSize);
            return View(lst_hd);
        }

        public ActionResult Export(string chuky, string khuvuc, int duong, string soghi, string tthoadon, string loaihoadon)
        {
            GridView gv = new GridView();
            SqlCommand cmd = new SqlCommand();
            var conn = Business.Connection.ConnectionString();
            conn.Open();
            db.Database.ExecuteSqlCommand("InsertHoadon @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}, @tthoadon={4}, @loaihoadon={5}", chuky, khuvuc, duong, soghi, tthoadon, loaihoadon);
            gv.DataSource = db.ExportHoadon().ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ExportHoadon.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "Utf-8";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            htw.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            gv.RenderControl(htw);
            //string style = @"<style> TD { mso-number-format:\@; } </style>";//Set format cell ve Text
            //Response.Write(style);//
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index");
        }

        public EmptyResult Thanhtoantatca(string chuky, string khuvuc, int duong, string soghi, string tthoadon, string loaihoadon, string httt)
        {
            db.Database.ExecuteSqlCommand("Thanhtoantatca @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}, @tthoadon={4}, @loaihoadon={5}, @httt={6}", chuky, khuvuc, duong, soghi, tthoadon, loaihoadon, httt);
            //List<getListHoaDon_Result> hoaDons = db.getListHoaDon(chuky, khuvuc, duong, soghi, tthoadon, loaihoadon, httt).ToList().Take(9000).ToList();
            //var result =await PhatHanhHDDT(hoaDons);

            return new EmptyResult();
        }

       
        public EmptyResult Tontatca(string chuky, string khuvuc, int duong, string soghi, string tthoadon, string loaihoadon, string httt)
        {
            db.Database.ExecuteSqlCommand("Tontatca @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}, @tthoadon={4}, @loaihoadon={5}, @httt={6}", chuky, khuvuc, duong, soghi, tthoadon, loaihoadon, httt);
            return new EmptyResult();
        }
        public JsonResult getHoadon(string dsmscd, string chuky)
        {
            var result = db.Database.SqlQuery<HOADON>("exec HoadonSelect @dsmscd={0}, @chuky={1}", dsmscd, chuky).ToList<HOADON>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public EmptyResult Chamhoadon(string dsmscd, string[] chuky, string trangthai, string chukyht)
        {
            var i = 0;
            for (i = 0; i < chuky.Count(); i++)
            {
                db.Database.ExecuteSqlCommand("Chamhoadon @dsmscd={0},@chuky={1},@trangthai={2}, @chukyht={3}", dsmscd, chuky[i], trangthai, chukyht);
            }
            return new EmptyResult();
        }
       
        
        public string UnconfirmHoaDons()
        {
            var keys = new List<string>();
            var hddts = db.INVOICEs.ToList();
            foreach (var hddt in hddts)
            {
                keys.Add(hddt.Key.ToString());
            }
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            string lstFkey = "";
            foreach (var key in keys)
            {
                lstFkey += key + "_";
            }
            lstFkey = lstFkey.Substring(0, lstFkey.Length - 1);
            string result = businessServiceClient.unConfirmPayment(lstFkey, Username, Password);
            return result;
        }
        public async Task<string> CancelHoaDons()
        {
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            var invoices = db.INVOICEs.ToList().Skip(300).Take(100);
            List<Task> tasks = new List<Task>();
            var results = new List<string>();
            string result = "";
            foreach (var invoice in invoices)
            {
                Task task = Task.Run(() =>{
                    try {
                        results.Add(businessServiceClient.cancelInv(Account, ACpass, invoice.Key.ToString(), Username, Password)+":"+ invoice.Key.ToString());
                    } catch (Exception ex) {
                        results.Add("TimeOut: "+ invoice.Key.ToString());
                    };
                    
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);
            foreach(var _result in results)
            {
                result += _result + ", ";
            }
            return result;
        }
    }
}