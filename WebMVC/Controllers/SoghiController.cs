﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class SoghiController : Controller
    {
        //
        // GET: /Soghi/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_kv = db.KhuVucSelect();
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View(lst_kv);
        }
        public ActionResult Soghi(string khuvuc, string currentFilter, string searchString, int? page)
        {
            if (searchString != null && searchString != "")
            {page = 1;}
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;
            ViewBag.DuongList = new SelectList(db.getDuongbyKhuvuc(khuvuc,""), "IDDUONG", "TENDUONG");
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.khuvuc = khuvuc;          
            ViewBag.tenkhuvuc = db.getKhuvucbyMakhuvuc(khuvuc).Select(a => a.TENKHUVUC).FirstOrDefault();
            var lst_sg = db.Database.SqlQuery<SOGHI>("getSoghibyKhuvuc @i_makhuvuc={0}, @i_searchstring={1}", khuvuc, searchString).ToPagedList(pageNumber, pageSize);
            return View(lst_sg);
        }
        public JsonResult getDuong(string khuvuc)
        {
            var result = db.getDuongbyKhuvuc(khuvuc,"");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ThemSoghi(string masoghi, string tensoghi, int tustt, int denstt, int idduong)
        {
            db.Database.ExecuteSqlCommand("InsertSoghi @masoghi={0}, @tensoghi={1}, @tustt={2}, @denstt={3}, @idduong={4}", masoghi, tensoghi, tustt, denstt, idduong);
            return new EmptyResult();
        }
        public EmptyResult UpdateSoghi(string masoghi, string tensoghi)
        {
            db.Database.ExecuteSqlCommand("UpdateSoghi @masoghi={0}, @tensoghi={1}", masoghi, tensoghi);
            return new EmptyResult();
        }

        public EmptyResult XoaSoghi (string id)
        {
            db.Database.ExecuteSqlCommand("DeleteSoghi @masoghi={0}", id);
            return new EmptyResult();
        }
        
    }
}