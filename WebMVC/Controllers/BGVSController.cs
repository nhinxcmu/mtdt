﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class BGVSController : Controller
    {
        //
        // GET: /BGVS/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_bgvs = db.Database.SqlQuery<BANGGIAVESINH>("Exec GetBGVS_Cond").ToList();
            return View(lst_bgvs);
        }
        public EmptyResult ThemBGVS(string tenbgvs, decimal thanhtienvs)
        {
            db.Database.ExecuteSqlCommand("InsertBGVS @tenbgvs={0}, @thanhtienvs={1}", tenbgvs, thanhtienvs);
            return new EmptyResult();
        }
        public EmptyResult UpdateBGVS(int idbgvs, string tenbgvs, decimal thanhtienvs)
        {
            db.Database.ExecuteSqlCommand("UpdateBGVS @idbgvs={0}, @tenbgvs={1}, @thanhtienvs={2}", idbgvs, tenbgvs, thanhtienvs);
            return new EmptyResult();
        }   
     
        public EmptyResult XoaBGVS(int id)
        {
            db.Database.ExecuteSqlCommand("DeleteBGVS @idbgvs={0}", id);
            return new EmptyResult();
        }
	}
}