﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections;
using Data;
using Business;

namespace WebMVC.Controllers
{
    public class ExportController : Controller
    {
        private MTDTEntities db = new MTDTEntities();
        //
        // GET: /Export/
        public ActionResult Index()
        {
            var lst_kv = db.KHUVUCs.Where(a=>a.IDHUYEN == 1);
            return View(lst_kv.ToList());
        }
        public JsonResult getDuong(string khuvuc)
        {
            var result = (from r in db.DUONGs where r.MAKHUVUC == khuvuc select new { r.IDDUONG, r.TENDUONG}).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSoghi(int duong)
        {
            var result = (from r in db.SOGHIs where r.IDDUONG == duong select new { r.MASOGHI, r.TENSOGHI }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Export(string khuvuc, int duong, string soghi)
        {
            
            GridView gv = new GridView();
            if (khuvuc != "0" && duong == 0 && soghi == "0")
                gv.DataSource = db.KHACHHANGs.Where(a=>a.MAKHUVUC == khuvuc).ToList();
            else if (khuvuc !="0" && duong != 0 && soghi == "0")
                gv.DataSource = db.KHACHHANGs.Where(a=>a.MAKHUVUC == khuvuc && a.IDDUONG == duong).ToList();
            else if (khuvuc != "0" && duong != 0 && soghi != "0")
                gv.DataSource = db.KHACHHANGs.Where(a => a.MAKHUVUC == khuvuc && a.IDDUONG == duong && a.MASOGHI == soghi).ToList();
            //gv.DataSource = db.KHACHHANGs.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ExportKhachhang.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "Utf-8";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            htw.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"); 
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }

	}

}