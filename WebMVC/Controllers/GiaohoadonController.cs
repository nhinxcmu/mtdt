﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;
using Business;

namespace WebMVC.Controllers
{
    public class GiaohoadonController : Controller
    {
        //
        // GET: /Giaohoadon/
        private MTDTEntities db = new MTDTEntities();

        [Authorize]
        public ActionResult Index()
        {
            //ViewBag.KhuvucList = new SelectList(db.KhuVucSelect(), "MAKHUVUC", "TENKHUVUC");
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View(Business.KHACHHANG.Index());
            /*if(!User.IsInRole("Administrator"))
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);*/
        }
        //Xu ly Ajax dropdownlist
        public JsonResult getChukybyNam(string nam)
        {
            var result = db.getChukybyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getKhuvuc(string chuky)
        {
            var result = db.getKhuvucbyChuky(chuky);
            return Json(result, JsonRequestBehavior.AllowGet);
        }        
        public JsonResult getSoghi(string khuvuc, string chuky)
        {  
            var result = db.getSoghiGiaohoadon(chuky, khuvuc);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLangiaobyMakhuvuc(string chuky, string khuvuc)
        {
            var result = db.getLangiaobyMakhuvuc(chuky, khuvuc);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public EmptyResult Giaohoadon(string chuky, string khuvuc, string[] masoghi, int[] tuseri, int[] denseri, int langiao, int[] sohd, decimal[] sotien)
        {
            string tendangnhap = User.Identity.Name.ToString();
            var i = 0;
            for (i = 0; i< masoghi.Count() ; i++)
            {
                db.Database.ExecuteSqlCommand("InsertGiaohoadon @chuky={0},@langiao={1},@soghi={2},@tuseri={3},@denseri={4},@tendangnhap={5},@sohd={6},@sotien={7}", chuky, langiao, masoghi[i], tuseri[i], denseri[i], tendangnhap, sohd[i], sotien[i]);
            }
            return new EmptyResult();
            //return Content("<script language='javascript' type='text/javascript'>alert('Lưu thành công');window.location.href='~/../Index'</script>");
        }
        [Authorize]
        public ActionResult Xoachinhhoadon()
        {
            //ViewBag.ChukyList = new SelectList((from a in db.GIAOHOADONs select new {CHUKY = a.CHUKY, TENCHUKY = a.CHUKY.Substring(4,2)+"/"+a.CHUKY.Substring(0,4)}).Distinct(), "CHUKY", "TENCHUKY");
            //ViewBag.ChukyList = new SelectList(db.getChukyGiaohoadon(), "CHUKY", "TENCHUKY");
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View();
        }
        public JsonResult getSoghi_Xoachinh(string chuky, string khuvuc, int langiao)
        {
            var result = db.getSoghiXoachinh(chuky, khuvuc, langiao);
            return Json(result, JsonRequestBehavior.AllowGet);
        }        
        public EmptyResult Luuxoachinh(string[] idctghd, int[] xoachinhhd, decimal[] xoachinhst, int[] tuseri, int[] denseri, int[] giaohd, decimal[] giaost)
        {
            var i = 0;
            for (i = 0; i < idctghd.Count(); i++)
            {
                db.Database.ExecuteSqlCommand("Xoachinhhoadon @idctghd={0},@xoachinhhd={1},@xoachinhst={2}, @tuseri={3}, @denseri={4}, @giaohd={5}, @giaost={6}", idctghd[i], xoachinhhd[i], xoachinhst[i], tuseri[i], denseri[i], giaohd[i], giaost[i]);
            }
            return new EmptyResult();
            //return Content("<script language='javascript' type='text/javascript'>alert('Lưu thành công');window.location.href='~/../Xoachinhhoadon'</script>");
        }
        public EmptyResult Xoa(int idctghd)
        {
            db.Database.ExecuteSqlCommand("DeleteCTGHD @idctghd={0}", idctghd);
            return new EmptyResult();
        }

	}
}