﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.OleDb;
using System.Data.SqlClient;
using Data;

namespace WebMVC.Controllers
{
    public class ImportExcelController : Controller
    {
        private MTDTEntities db = new MTDTEntities();
        [Authorize]
        public ActionResult Import()
        {
            //var lst_kv = (from a in db.KHUVUCs where string.Equals(a.MAKHUVUC, "01") || string.Equals(a.MAKHUVUC, "02") || string.Equals(a.MAKHUVUC, "03") || string.Equals(a.MAKHUVUC, "04") select a).ToList();
            var lst_kv = db.Database.SqlQuery<KHUVUC>("Exec KhuvucSelect");
            if (User.IsInRole("ReadOnly"))
                return Content("<script language='javascript' type='text/javascript'>alert('Bạn không có quyền thực hiện thao tác này')</script>");
            else
                return View(lst_kv);
        }
        public ActionResult getChuky(string khuvuc)
        {            
            var result = db.getChukybyMakhuvuc(khuvuc).Select(a => a.chuky).Max();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult ImportExcel(string khuvuc)
        {
            //public ActionResult ImportExcel (string khuvuc)
            string path1 = "";
            if (Request.Files["Fileupload1"].ContentLength > 0)//Co nhap file
            {
                var conn = Business.Connection.ConnectionString();//Tao ket noi database
                conn.Open();
                string filename = System.IO.Path.GetFileName(Request.Files["FileUpload1"].FileName);
                path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadExcel"), filename);//Noi luu file
                
                if (System.IO.File.Exists(path1))
                {
                    try
                    {
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();                        
                        System.IO.File.Delete(path1);                        
                        //Request.Files["FileUpload1"].SaveAs(path1);//Luu file                                              
                    }
                    catch (System.IO.IOException)
                    {
                        conn.Close();
                        conn.Open();
                    }                    
                }
                Request.Files["FileUpload1"].SaveAs(path1);//Luu file               
                string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";//Chuoi ket noi Oledb
                //string excelConnectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", path1);
                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString); //Tao ket noi Oledb                        
                //excelConnection.Open();
                db.Database.ExecuteSqlCommand("ImportKhachhang_1 @makhuvuc={0}", khuvuc);                

                SqlBulkCopy sqlBulk = new SqlBulkCopy(Business.Connection.getConnectionString());
               
                //BangHTTT
                try 
                {
                    excelConnection.Open();
                    OleDbCommand cmdhttt = new OleDbCommand("Select [mahttt],[tenhttt] from [hinhthuctt$]", excelConnection);
                    OleDbDataReader dReaderhttt = cmdhttt.ExecuteReader();
                    sqlBulk.DestinationTableName = "HINHTHUCTT_tam";
                    sqlBulk.WriteToServer(dReaderhttt);
                    db.Database.ExecuteSqlCommand("ImportHTTT");  
                }
                catch
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "HINHTHUCTT_TAM");
                }
                finally
                {                    
                    excelConnection.Close();
                }

                //Bang Huyen
                try
                {
                    excelConnection.Open();//Mo ket noi Oledb                   
                    OleDbCommand cmdh = new OleDbCommand("Select [idhuyen],[idtinh],[tenhuyen] from [huyen$]", excelConnection);
                    OleDbDataReader dReaderh = cmdh.ExecuteReader();
                    sqlBulk.DestinationTableName = "huyen_tam";
                    sqlBulk.WriteToServer(dReaderh);
                    db.Database.ExecuteSqlCommand("ImportHuyen");
                }
                catch 
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "HUYEN_TAM");
                }                
                finally
                {                    
                    excelConnection.Close();// Dong ket noi Oledb
                    //excelConnection.Dispose();
                }
                        
                             
                //Bang Dacdiem
                try
                {
                    excelConnection.Open();
                    OleDbCommand cmddd = new OleDbCommand("Select [madacdiem],[tendacdiem] from [dacdiem$]", excelConnection);
                    OleDbDataReader dReaderdd = cmddd.ExecuteReader();
                    sqlBulk.DestinationTableName = "dacdiem_tam";
                    sqlBulk.WriteToServer(dReaderdd);
                    db.Database.ExecuteSqlCommand("ImportDacdiem"); 
                }
                catch 
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "DACDIEM_TAM");
                }
                finally
                {                    
                    excelConnection.Close();
                    //excelConnection.Dispose();
                }

                //Bang Khuvuc
                try
                {
                    excelConnection.Open();
                    OleDbCommand cmdkv = new OleDbCommand("Select [makhuvuc],[idphongban],[idhuyen], [tenkhuvuc],[dienthoai],CInt([thupbvmt]) from [khuvuc$]", excelConnection);
                    OleDbDataReader dReaderkv = cmdkv.ExecuteReader();
                    sqlBulk.DestinationTableName = "khuvuc_tam";
                    sqlBulk.WriteToServer(dReaderkv);
                    db.Database.ExecuteSqlCommand("ImportKhuvuc"); 
                }
                catch 
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "KHUVUC_TAM");
                }
                finally
                {                    
                    excelConnection.Close();
                    //excelConnection.Dispose();
                }
                
                //Bang Duong  
                try
                {
                    excelConnection.Open();
                    OleDbCommand cmdd = new OleDbCommand("Select [idduong],[idkhomap],[makhuvuc],[tenduong] from [duong$]", excelConnection);
                    OleDbDataReader dReaderd = cmdd.ExecuteReader();
                    sqlBulk.DestinationTableName = "duong_tam";
                    sqlBulk.WriteToServer(dReaderd);
                    db.Database.ExecuteSqlCommand("ImportDuong");  
                }
                catch
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "DUONG_TAM");
                }
                finally
                {                    
                    excelConnection.Close();
                    //excelConnection.Dispose();
                }

                //Bang Soghi
                try
                {
                    excelConnection.Open();
                    OleDbCommand cmdsg = new OleDbCommand("Select [masoghi],[idduong],[tensoghi],[tustt],[denstt] from [soghi$]", excelConnection);
                    OleDbDataReader dReadersg = cmdsg.ExecuteReader();
                    sqlBulk.DestinationTableName = "soghi_tam";
                    sqlBulk.WriteToServer(dReadersg);
                    db.Database.ExecuteSqlCommand("ImportSoghi");  
                }
                catch
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "SOGHI_TAM");
                }
                finally
                {                    
                    excelConnection.Close();
                    //excelConnection.Dispose();
                }
                //Bang Khachhang             
                try
                {
                    excelConnection.Open();
                    OleDbCommand cmdkh = new OleDbCommand("Select null,[masoghi],[idduong],[makhuvuc],[mahttt],[madacdiem],[danhso],[masocd],[hoten],[sonha],[masothue],Cdbl([thanhtienvs])*10/11,Cdbl([thanhtienvs])/11,CDbl([thanhtienvs]),null,null,null,null,null,null,[nhanvienthu],null,null from [khachhang$]", excelConnection);
                    OleDbDataReader dReaderkh = cmdkh.ExecuteReader();
                    sqlBulk.DestinationTableName = "khachhang_tam";
                    sqlBulk.WriteToServer(dReaderkh);
                    db.Database.ExecuteSqlCommand("ImportKhachhang_2 @makhuvuc={0}", khuvuc);             
                }
                catch (SqlException ex)
                {
                    db.Database.ExecuteSqlCommand("DropTablename @tablename={0}", "KHACHHANG_TAM");
                }
                finally
                { 
                     excelConnection.Close();                     
                }                
                conn.Close();               
                
                //Request.Files["FileUpload1"].InputStream.Dispose();
                
            }           
            return Content("<script language='javascript' type='text/javascript'>alert('Import thành công');window.location.href='~/../Import'</script>");
        }
    }
}