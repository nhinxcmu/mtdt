﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;
using Data.ViewModels;

namespace WebMVC.Controllers
{
    public class ChuyenkhachhangController : Controller
    {
        // GET: Chuyenkhachhang
        private MTDTEntities db = new MTDTEntities();

        [Authorize]
        [Authorize]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Home");
            //ViewBag.ChukyList = new SelectList(db.GetChuky(), "CHUKY", "TENCHUKY");     
            ViewBag.NamList = new SelectList(db.GetNam(), "NAM", "NAM");
            return View(Business.KHACHHANG.Index());
        }
        //Xu ly Ajax dropdownlist
        public JsonResult getChukybyNam(string nam)
        {
            var result = db.getChukybyNam(nam); //call store procedure getKhuvucbyChuky duoc update to model
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getKhuvuc(string chuky)
        {
            var result = db.getKhuvucbyChuky(chuky);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDuong(string khuvuc)
        {
            var result = db.getDuongbyKhuvuc(khuvuc,"");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSoghi(int duong)
        {
            var result = db.getSoghibyDuong(duong);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Chuyenkhachhang(string chuky, string tuds, string dends, string khuvuc_chuyen, int duong_chuyen, string soghi_chuyen, string khuvuc_den, int duong_den, string soghi_den)
        {
            //db.Database.ExecuteSqlCommand("Chuyenkhachhang @chuky={0}, @tuds={1}, @dends ={2}, @khuvuc = {3}, @duong = {4}, @soghi = {5}", chuky, tuds, dends, khuvuc, duong, soghi);
            db.Database.ExecuteSqlCommand("Chuyenkhachhang @chuky={0}, @tuds={1}, @dends ={2}, @khuvuc = {3}, @duong = {4}, @soghi = {5}, @khuvuc_chuyen = {6}, @duong_chuyen = {7}, @soghi_chuyen = {8} ", chuky, tuds, dends, khuvuc_den, duong_den, soghi_den, khuvuc_chuyen, duong_chuyen, soghi_chuyen);
            return Content("<script language='javascript' type='text/javascript'>alert('Cập nhật thành công');window.location.href='~/../Chuyenkhachhang</script>");
        }
    }
}