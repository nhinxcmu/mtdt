﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class KhuvucController : Controller
    {
        //
        // GET: /HTTT/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_kv = db.KhuVucSelect().ToList();
            return View(lst_kv);
        }
        public EmptyResult ThemKhuvuc(string makhuvuc, string tenkhuvuc)
        {
            db.Database.ExecuteSqlCommand("InsertKhuvuc @makhuvuc={0}, @tenkhuvuc={1}", makhuvuc, tenkhuvuc);
            return new EmptyResult();
        }
        public EmptyResult UpdateKhuvuc(string makhuvuc, string tenkhuvuc)
        {
            db.Database.ExecuteSqlCommand("UpdateKhuvuc @makhuvuc={0}, @tenkhuvuc={1}", makhuvuc, tenkhuvuc);
            return new EmptyResult();
        }
        public EmptyResult Xoakhuvuc(string id)
        {
            db.Database.ExecuteSqlCommand("DeleteKhuvuc @makhuvuc={0}", id);
            return new EmptyResult();
        }        
	}
}