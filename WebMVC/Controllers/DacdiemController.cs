﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Data;


namespace WebMVC.Controllers
{
    public class DacdiemController : Controller
    {
        //
        // GET: /DACDIEM/
        private MTDTEntities db = new MTDTEntities();
        public ActionResult Index()
        {
            var lst_dd = db.GetDacdiem().ToList();
            return View(lst_dd);
        }
        public EmptyResult ThemDacdiem(string madacdiem, string tendacdiem)
        {
            db.Database.ExecuteSqlCommand("InsertDacdiem @madacdiem={0}, @tendacdiem={1}", madacdiem, tendacdiem);
            return new EmptyResult();
        }
        public EmptyResult UpdateDacdiem(string madacdiem, string tendacdiem)
        {
            db.Database.ExecuteSqlCommand("UpdateDacdiem @madacdiem={0}, @tendacdiem={1}", madacdiem, tendacdiem);
            return new EmptyResult();
        }

        public EmptyResult XoaDacdiem(string id)
        {
            db.Database.ExecuteSqlCommand("DeleteDacdiem @madacdiem={0}", id);
            return new EmptyResult();
        }
    }
}