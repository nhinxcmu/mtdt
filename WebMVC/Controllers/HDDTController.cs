﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using PagedList;
using Data.ViewModels;
using System.Threading.Tasks;
using WebMVC.InvoiceServices;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;

namespace WebMVC.Controllers
{
    public class HDDTController : Controller
    {
        MTDTEntities db = new MTDTEntities();
        private PublishService.PublishServiceSoapClient publishServiceClient = new PublishService.PublishServiceSoapClient();
        private BusinessService.BusinessServiceSoapClient businessServiceClient = new BusinessService.BusinessServiceSoapClient();
        private PortalService.PortalServiceSoapClient portalServiceSoapClient = new PortalService.PortalServiceSoapClient();
        string Username = "";
        string Password = "";
        string Account = "";
        string ACpass = "";
        string Pattern = "";
        string Serial = "";
        List<HINHTHUCTT> hinhThucs;
        List<BANGGIAVESINH> services;
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        #region Phát hành hóa đơn
        [Authorize]
        public ActionResult PhatHanh()
        {
            return View();
        }
        [Authorize]
        public ActionResult PhatHanhHoaDon(string chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi, string loaihoadon, string httt, string status, int lh28k)
        {
            db = new MTDTEntities();
            if (searchString != null)
            { page = 1; }
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;
            chuky = chuky == "0" ? "1" : chuky;
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);

            int pageSize = 50;
            if (pagesize.HasValue)
            {
                pageSize = (int)pagesize;
            }
            ViewBag.pagesize = pageSize;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.loaihoadon = loaihoadon;
            ViewBag.httt = httt;
            ViewBag.status = status;
            ViewBag.searchString = searchString;
            ViewBag.lh28k = lh28k;
            var lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", searchString, status, lh28k).ToPagedList(pageNumber, pageSize);
            ViewBag.HDDTs = lst_hd;
            return View("PhatHanh", lst_hd);
        }
        [Authorize]
        public async Task<string> PhatHanhHoaDons(string[] selectedHoadons)
        {
            List<GetHoaDonJoinLastInvoice> hoaDons = new List<GetHoaDonJoinLastInvoice>();
            List<SelectedHoaDon> _selectedHoadons = new List<SelectedHoaDon>();
            foreach (var hoadon in selectedHoadons)
            {
                var index = hoadon.IndexOf("|");
                string chuKyNo = hoadon.Substring(0, index);
                string msCoDinh = hoadon.Substring(index + 1);
                SelectedHoaDon selectedHoadon = new SelectedHoaDon(chuKyNo, msCoDinh);
                _selectedHoadons.Add(selectedHoadon);
            }

            foreach (var hoadon in _selectedHoadons)
            {
                var hd = db.TIMKIEMKHACHHANGs.SingleOrDefault(n => n.CHUKYNO == hoadon.chuKyNo && n.MASOCD == hoadon.msCoDinh);
                hoaDons.Add(new GetHoaDonJoinLastInvoice(hd));
            }
            string phatHanhResult = await PhatHanhHDDT(hoaDons);
            return phatHanhResult;

        }
        [Authorize]
        public async Task<string> PhatHanhTatCa(string chuky, string searchString, string khuvuc, int duong, string soghi, string status, int lh28k)
        {
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);
            var lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", searchString, status, lh28k).ToList();

            //string phatHanhResult = await PhatHanhHDDT(lst_hd.Skip(0).Take(2500).ToList());
            string phatHanhResult = await PhatHanhHDDT(lst_hd.ToList());
            return phatHanhResult;
            //return "";
        }
        [Authorize]
        public async Task<string> ThanhToanTatCa(string chuky, string searchString, string khuvuc, int duong, string soghi, string status, int lh28k)
        {
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);
            var lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", searchString, status, lh28k).ToList();

            //string phatHanhResult = await PhatHanhHDDT(lst_hd.Skip(0).Take(2500).ToList());
            List<string> keys = new List<string>();
            foreach(var hoaDon in lst_hd)
            {
                keys.Add(hoaDon.KEY);
            }
            List<string> confirmResults = await ConfirmHoaDons(keys);
            foreach(var key in confirmResults)
            {
                var khachHangInvoice = db.KHACHHANG_INVOICE.SingleOrDefault(n => n.KEY == key);
                khachHangInvoice.TRANGTHAI = "CONFIRMED";
                db.Entry(khachHangInvoice).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();
            return confirmResults.Count.ToString();
        }
        [Authorize]
        public async Task<string> PhatHanhHDDT(List<GetHoaDonJoinLastInvoice> hoaDons)
        {
            //------Import và Confirm những hóa đơn đã thanh toán-------//            
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            var soHoaDonPerTask = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "InvoicePerRequest").CONFIG_VALUE);
            var soTaskMoiLanGoiHam = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "RequestPerTime").CONFIG_VALUE); //Chưa dùng

            hinhThucs = db.HINHTHUCTTs.ToList();
            services = db.BANGGIAVESINHs.ToList();
            var invoices = new List<INVOICE>();
            List<string> confirmedKeys = new List<string>();

            var tasks = new List<Task>();

            if (hoaDons.Count > 0)
            {
                var soLuongTask = hoaDons.Count / soHoaDonPerTask; //Chia hết lấy được số task(request)
                var hoaDonDu = soLuongTask > 0 ? hoaDons.Count % soHoaDonPerTask : hoaDons.Count;        //Chia dư lấy được số lượng hóa đơn còn lại để chạy task cuối. Khi số lượng hóa đơn nhỏ hơn soHoaDonPerTask sẽ cho coi số lượng hóa đơn như hoaDonDu
                for (int i = 0; i < soLuongTask; i++)
                {
                    var _hoaDons = hoaDons.Skip(i * soHoaDonPerTask).Take(soHoaDonPerTask).ToList();
                    var _invoices = ImportHoaDons(_hoaDons);
                    if (_invoices.Count == 0) break;
                    invoices.AddRange(_invoices);
                    //var _importedKeys = new List<string>();
                    //foreach (var invoice in _invoices)
                    //{
                    //    _importedKeys.Add(invoice.Key.ToString());
                    //}
                }
                if (hoaDonDu > 0)
                {
                    var _hoaDons = hoaDons.Skip(hoaDons.Count - hoaDonDu).Take(hoaDonDu).ToList();
                    var _invoices = ImportHoaDons(_hoaDons);
                    if (_invoices.Count > 0)
                    invoices.AddRange(_invoices);
                    //var _importedKeys = new List<string>();
                    //foreach (var invoice in _invoices)
                    //{
                    //    _importedKeys.Add(invoice.Key.ToString());
                    //}
                }
                try
                {
                    await Task.WhenAll(tasks);
                }
                catch (Exception ex)
                {

                }

                db.INVOICEs.AddRange(invoices);
                db.SaveChanges();
            }
            return invoices.Count + "/" + hoaDons.Count;
        }
        public INVOICE khoiTaoHoaDonLuuDb(GetHoaDonJoinLastInvoice hoaDon)
        {
            var VATRate = 8;
            var chuKyNo = int.Parse(hoaDon.CHUKYNO);
            var key = "" + hoaDon.MASOCD + hoaDon.CHUKYNO + db.KHACHHANG_INVOICE.Where(n => n.CHUKYNO == chuKyNo && n.MASOCD == hoaDon.MASOCD).ToList().Count();
            var invoice = new INVOICE();
            var service = services.SingleOrDefault(n => n.IDBGVS == hoaDon.IDBGVS);
            invoice.Key = key;
            invoice.Amount = hoaDon.THANHTIENVS;
            invoice.AmountInwords = CongCu.So_chu(double.Parse(hoaDon.THANHTIENVS.ToString()));
            invoice.ArisingDate = DateTime.Now.Date;
            string soNha = hoaDon.SONHA == "" ? "" : hoaDon.SONHA + ", ";
            string diaChi = soNha + hoaDon.TENSOGHI + ", TP.Cà Mau";
            THONGTINKHACHHANG ttkh = db.THONGTINKHACHHANGs.SingleOrDefault(n => n.MASOCD == hoaDon.MASOCD);
            if (ttkh != null)
            {
                if (ttkh.DIA_CHI != null & ttkh.DIA_CHI != "")
                    diaChi = ttkh.DIA_CHI;
            }

            invoice.CusAddress = diaChi;
            invoice.CusBankNo = "";
            invoice.CusCode = hoaDon.DANHSO;
            invoice.CusName = hoaDon.HOTEN;
            invoice.CusPhone = hoaDon.SDT;
            invoice.CusTaxCode = hoaDon.MASOTHUE;
            invoice.InvoiceName = "HÓA ĐƠN DỊCH VỤ VỆ SINH ";
            invoice.InvoiceNo = hoaDon.DANHSO;
            invoice.InvoicePattern = Pattern;
            invoice.KindOfService = service.TENBGVS.ToUpper();
            invoice.PaymentMethod = hoaDon.MAHTTT == null ? "TM" : hinhThucs.SingleOrDefault(n => n.MAHTTT == hoaDon.MAHTTT).TENHTTT;
            invoice.SerialNo = Serial;
            //SignDate = DateTime.Now,
            invoice.Total = hoaDon.TIENVS;
            invoice.VATRate = VATRate;
            invoice.VATAmount = hoaDon.THUE;
            invoice.DiscountAmount = 0;
            invoice.PaymentStatus = 0;
            return invoice;
        }
        [Authorize]
        public List<INVOICE> ImportHoaDons(List<GetHoaDonJoinLastInvoice> hoaDons)
        {
            MTDTEntities dbForCount = new MTDTEntities();
            xmlInvDataImportAndPublish data = new xmlInvDataImportAndPublish();
            data.Invoices = new List<ImportAndPublishInv>();
            //List<string> keys = new List<string>();
            var VATRate = 8;
            var invoices = new List<INVOICE>();
            var khachHangInvoices = new List<KHACHHANG_INVOICE>();
            publishServiceClient.Endpoint.Binding.SendTimeout = new TimeSpan(0, 15, 0);
            foreach (var hoadon in hoaDons)
            {
                var chuKyNo = int.Parse(hoadon.CHUKYNO);
                var key = "" + hoadon.MASOCD + hoadon.CHUKYNO + dbForCount.KHACHHANG_INVOICE.Where(n => n.CHUKYNO == chuKyNo && n.MASOCD == hoadon.MASOCD).ToList().Count();
                ////keys.Add(key);
                //var invoice = new INVOICE();
                //var service = services.SingleOrDefault(n => n.IDBGVS == hoadon.IDBGVS);
                //invoice.Key = key;
                //invoice.Amount = hoadon.THANHTIENVS;
                //invoice.AmountInwords = CongCu.So_chu(double.Parse(hoadon.THANHTIENVS.ToString()));
                //invoice.ArisingDate = DateTime.Now.Date;
                //string soNha = hoadon.SONHA == "" ? "" : hoadon.SONHA + ", ";
                //string diaChi = soNha + hoadon.TENSOGHI + ", TP.Cà Mau";
                //THONGTINKHACHHANG ttkh = db.THONGTINKHACHHANGs.SingleOrDefault(n => n.MASOCD == hoadon.MASOCD);
                //if (ttkh != null)
                //{
                //    if (ttkh.DIA_CHI != null & ttkh.DIA_CHI != "")
                //        diaChi = ttkh.DIA_CHI;
                //}

                //invoice.CusAddress = diaChi;
                //invoice.CusBankNo = "";
                //invoice.CusCode = hoadon.DANHSO;
                //invoice.CusName = hoadon.HOTEN;
                //invoice.CusPhone = hoadon.SDT;
                //invoice.CusTaxCode = hoadon.MASOTHUE;
                //invoice.InvoiceName = "HÓA ĐƠN DỊCH VỤ VỆ SINH ";
                //invoice.InvoiceNo = hoadon.DANHSO;
                //invoice.InvoicePattern = Pattern;
                //invoice.KindOfService = service.TENBGVS.ToUpper();
                //invoice.PaymentMethod = hoadon.MAHTTT == null ? "TM" : hinhThucs.SingleOrDefault(n => n.MAHTTT == hoadon.MAHTTT).TENHTTT;
                //invoice.SerialNo = Serial;
                ////SignDate = DateTime.Now,
                //invoice.Total = hoadon.TIENVS;
                //invoice.VATRate = VATRate;
                //invoice.VATAmount = hoadon.THUE;
                //invoice.DiscountAmount = 0;
                //invoice.PaymentStatus = 0;
                INVOICE invoice = khoiTaoHoaDonLuuDb(hoadon);
                invoices.Add(invoice);

                var products = new List<Product>();
                products.Add(new Product
                {
                    Amount = hoadon.TIENVS.ToString(),
                    //ProdName = service.TENBGVS,
                    ProdName = "Dịch vụ vệ sinh tháng " + hoadon.CHUKYNO.Substring(4) + " năm " + hoadon.CHUKYNO.Substring(0, 4) + " " + hoadon.GHICHU,
                    ProdPrice = hoadon.TIENVS.ToString(),
                    ProdQuantity = "1",
                    ProdUnit = "Tháng"
                });


                ImportAndPublishInv hddt = new ImportAndPublishInv()//VNPT Invoice
                {
                    key = key,
                    Invoice = new Invoice(invoice, products),
                };
                data.Invoices.Add(hddt);
            }
            
            string xml = data.ToXML();
            try
            {
                SERVICE_CALL service = new SERVICE_CALL();
                service.CALL_TIME = DateTime.Now;
                service.FUNCTION_NAME = "ImportAndPublishInv";
                service.XML = xml;
                string serviceResult = publishServiceClient.ImportAndPublishInv(Account, ACpass, xml, Username, Password, Pattern, Serial, 0);
                service.RESPONSE = serviceResult;
                db.SERVICE_CALL.Add(service);
                db.SaveChanges();
                if (serviceResult.Contains("ERR"))
                {
                    return new List<INVOICE>();
                }
                foreach (var hoaDon in hoaDons)
                {
                    var chuKyNo = int.Parse(hoaDon.CHUKYNO);
                    var key = "" + hoaDon.MASOCD + hoaDon.CHUKYNO + dbForCount.KHACHHANG_INVOICE.Where(n => n.CHUKYNO == chuKyNo && n.MASOCD == hoaDon.MASOCD).ToList().Count();
                    KHACHHANG_INVOICE khachHangInvoice = new KHACHHANG_INVOICE();
                    khachHangInvoice.CHUKYNO = chuKyNo;
                    khachHangInvoice.KEY = key;
                    khachHangInvoice.MASOCD = hoaDon.MASOCD;
                    khachHangInvoice.NGAYTAO = DateTime.Now;
                    khachHangInvoice.NGAYUPDATE = DateTime.Now;
                    khachHangInvoice.PHAT_HANH_CHUNG = false;
                    khachHangInvoice.TRANGTHAI = "PUBLISHED";
                    db.KHACHHANG_INVOICE.Add(khachHangInvoice);
                }

                //OK:01GTKT0/001;VS/22E-010335992017090_201,010034762017090_202,010034722017090_203,010034712017090_204
                string okKeyNo = serviceResult.Substring(serviceResult.IndexOf('-') + 1);
                string[] keyNos = okKeyNo.Split(',');
                foreach (string keyNo in keyNos)
                {
                    string key = keyNo.Substring(0, keyNo.IndexOf('_'));
                    string no = keyNo.Substring(keyNo.IndexOf('_') + 1);
                    invoices.Single(n => n.Key == key).InvoiceNo = no;
                }

            }
            catch (Exception e)
            {
                return new List<INVOICE>();
            }
            return invoices;
        }
        #endregion

        #region Hủy hóa đơn
        [Authorize]
        public ActionResult Huy()
        {
            return View();
        }
        [Authorize]
        public ActionResult HuyHoaDon(string chuKyNo, int? page, int? pageSize, string khuvuc, int duong, string soghi, string trangThai, string key, string serialNo, string invoicePattern, string maSoCD)
        {
            page = page == null ? 1 : page;
            pageSize = pageSize == null ? 10 : pageSize;
            ViewBag.chuKyNo = chuKyNo;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.trangThai = trangThai;
            ViewBag.key = key;
            ViewBag.serialNo = serialNo;
            ViewBag.invoicePattern = invoicePattern;
            ViewBag.maSoCD = maSoCD;

            var lst_hd = db.Database.SqlQuery<InvoiceGetList>("Exec GetInvoices {0},{1},{2},{3},{4},{5},{6},{7},{8}", chuKyNo, serialNo, invoicePattern, maSoCD, duong, soghi, trangThai, key, -1).ToPagedList((int)page, (int)pageSize);
            ViewBag.HDDTs = lst_hd;
            return View("Huy", lst_hd);
        }
        public async Task<int> HuyHoaDons(string[] selectedInvoices)
        {
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;

            var canceledInvoices = await CancelHoaDons(selectedInvoices.ToList());
            foreach (var canceledInvoice in canceledInvoices)
            {
                var invoices = db.KHACHHANG_INVOICE.Where(n => n.KEY == canceledInvoice).ToList();
                foreach (KHACHHANG_INVOICE invoice in invoices)
                {
                    invoice.TRANGTHAI = "CANCELED";
                }
            }
            db.SaveChanges();
            return canceledInvoices.Count;
        }
        //public async Task<int> HuyTatCa(string chuKyNo, string khuvuc, int duong, string soghi, string trangThai, string key, string serialNo, string invoicePattern, string maSoCD)
        //{
        //    List<string> keys = new List<string>();
        //    var invoices = db.Database.SqlQuery<InvoiceGetList>("Exec GetInvoices {0},{1},{2},{3},{4},{5},{6},{7}", chuKyNo, serialNo, invoicePattern, maSoCD, duong, soghi, trangThai, key).ToList();
        //    foreach (var invoice in invoices)
        //    {
        //        keys.Add(invoice.Key);
        //    }
        //    int soHoaDonDaHuy = await HuyHoaDons(keys.ToArray());
        //    return 0;
        //}
        #endregion

        #region Get danh mục
        [Authorize]
        public string GetChuKyByNam(string nam)
        {
            var lst_hd = db.Database.SqlQuery<ChuKy>("Exec GetChuKyByNam_Invoice {0}", nam).ToList();
            return JsonConvert.SerializeObject(lst_hd);
        }
        //[Authorize]
        //public string GetInvoices(string maSoCD, int? chuKyNo)
        //{
        //    var invoices = db.GetInvoices(maSoCD, chuKyNo);
        //    return JsonConvert.SerializeObject(invoices);
        //}
        [Authorize]
        public string GetSerialNoByChuKy()
        {
            List<string> serialNos = db.INVOICEs.Select(n => n.SerialNo).Distinct().OrderBy(n => n).ToList();
            return JsonConvert.SerializeObject(serialNos);
        }
        [Authorize]
        public string GetInvoicePatternByChuKy(string chuKy)
        {
            List<string> invoicePatterns = db.INVOICEs.Select(n => n.InvoicePattern).Distinct().OrderBy(n => n).ToList();
            return JsonConvert.SerializeObject(invoicePatterns);
        }
        [Authorize]
        public string GetKhuVucByChuKy(string chuKy)
        {
            var khuVucs = db.getKhuvucbyChuky(chuKy);
            return JsonConvert.SerializeObject(khuVucs);
        }
        [Authorize]
        public string GetDuongByKhuVuc(string khuVuc)
        {
            var duongs = db.getDuongbyKhuvuc(khuVuc, "");
            return JsonConvert.SerializeObject(duongs);
        }
        [Authorize]
        public string GetSoGhiByDuong(int? duong)
        {
            var duongs = db.getSoghibyDuong(duong);
            return JsonConvert.SerializeObject(duongs);
        }
        #endregion



        [Authorize]
        public ActionResult GetList(string chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi, string tthoadon, string loaihoadon, string httt)
        {

            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.tthoadon = tthoadon;
            ViewBag.loaihoadon = loaihoadon;
            ViewBag.httt = httt;
            if (searchString != null)
            { page = 1; }
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;

            int pageSize = 30;
            if (pagesize.HasValue)
            {
                pageSize = (int)pagesize;
            }
            int pageNumber = (page ?? 1);

            ViewBag.pagesize = pageSize;
            ViewBag.CurrentPage = page;
            ViewBag.HDDTs = db.INVOICEs.ToList().ToPagedList(pageNumber, pageSize);
            return View();
        }
        [Authorize]
        public async Task<List<string>> ConfirmHoaDons(List<string> keys)
        {
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            var soHoaDonPerTask = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "InvoicePerRequest").CONFIG_VALUE);
            var soTaskMoiLanGoiHam = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "RequestPerTime").CONFIG_VALUE);
            var soLuongTask = keys.Count / soHoaDonPerTask; //Chia hết lấy được số task(request)
            var hoaDonDu = soLuongTask > 0 ? keys.Count % soHoaDonPerTask : keys.Count;        //Chia dư lấy được số lượng hóa đơn còn lại để chạy task cuối. Khi số lượng hóa đơn nhỏ hơn soHoaDonPerTask sẽ cho coi số lượng hóa đơn như hoaDonDu, chạy dòng 160
            List<string> confirmedInvoices = new List<string>();
            var tasks = new List<Task>();
            for (int i = 0; i < soLuongTask; i++)
            {
                var _keys = keys.Skip(i * soHoaDonPerTask).Take(soHoaDonPerTask).ToList();
                Task task = Task.Run(() =>
                {
                    string lstFkey = "";
                    foreach (var key in _keys)
                    {
                        lstFkey += key + "_";
                    }
                    lstFkey = lstFkey.Substring(0, lstFkey.Length - 1);
                    string result = businessServiceClient.confirmPaymentFkey(lstFkey, Username, Password);
                    if (!result.Contains("ERR"))
                    {
                        confirmedInvoices.AddRange(_keys);
                    }
                    else
                    {

                    }
                });
                tasks.Add(task);
            }
            if (hoaDonDu > 0)
            {
                var _keys = keys.Skip(keys.Count - hoaDonDu).Take(hoaDonDu).ToList();
                Task task = Task.Run(() =>
                {
                    string lstFkey = "";
                    foreach (var key in _keys)
                    {
                        lstFkey += key + "_";
                    }
                    lstFkey = lstFkey.Substring(0, lstFkey.Length - 1);
                    string result = businessServiceClient.confirmPaymentFkey(lstFkey, Username, Password);
                    if (!result.Contains("ERR"))
                    {
                        confirmedInvoices.AddRange(_keys);
                    }
                });
                tasks.Add(task);
            }
            try
            {
                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {

            }
            return confirmedInvoices;
        }
        [Authorize]
        public async Task<List<string>> CancelHoaDons(List<string> keys)
        {
            List<string> canceledKeys = new List<string>();
            List<string> unconfirmedKeys = new List<string>();

            var soTaskMoiLanGoiHam = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "RequestPerTime").CONFIG_VALUE); //Chưa dùng
            var soLanGoiHam = keys.Count / soTaskMoiLanGoiHam;
            var soKeyDu = soTaskMoiLanGoiHam == 0 ? keys.Count : keys.Count % soTaskMoiLanGoiHam;

            for (int i = 0; i < soLanGoiHam; i++)
            {
                List<Task> tasks = new List<Task>();
                foreach (var key in keys)
                {
                    Task task = Task.Run(() =>
                    {
                        SERVICE_CALL serviceCall = new SERVICE_CALL();
                        serviceCall.CALL_TIME = DateTime.Now;
                        serviceCall.FUNCTION_NAME = "cancelInv";
                        serviceCall.XML = key;
                        string result = businessServiceClient.cancelInv(Account, ACpass, key, Username, Password);
                        serviceCall.RESPONSE = result;
                        db.SERVICE_CALL.Add(serviceCall);
                        if (!result.Contains("ERR"))
                        {
                            canceledKeys.Add(key);
                        }
                        else if (result.Contains("9"))
                        {
                            var unconfirmedKey = UncomfirmHoaDon(key);
                            if (unconfirmedKey != "")
                            {
                                unconfirmedKeys.Add(unconfirmedKey);
                            }
                        }
                    });
                    tasks.Add(task);
                }
                await Task.WhenAll(tasks);
            }
            if (soKeyDu > 0)
            {
                List<Task> tasks = new List<Task>();
                foreach (var key in keys)
                {
                    SERVICE_CALL serviceCall = new SERVICE_CALL();
                    serviceCall.CALL_TIME = DateTime.Now;
                    serviceCall.FUNCTION_NAME = "cancelInv";
                    serviceCall.XML = key;
                    string result = businessServiceClient.cancelInv(Account, ACpass, key, Username, Password);
                    serviceCall.RESPONSE = result;
                    db.SERVICE_CALL.Add(serviceCall);
                    if (!result.Contains("ERR"))
                    {
                        canceledKeys.Add(key);
                    }
                    else if (result.Contains("9"))
                    {
                        var unconfirmedKey = UncomfirmHoaDon(key);
                        if (unconfirmedKey != "")
                        {
                            unconfirmedKeys.Add(unconfirmedKey);
                        }
                    }
                }
                await Task.WhenAll(tasks);
            }

            if (unconfirmedKeys.Count > 0)
            {
                canceledKeys.AddRange(await CancelHoaDons(unconfirmedKeys));
            }

            return canceledKeys;
        }
        [Authorize]
        public string UncomfirmHoaDon(string key)
        {
            var unconfirmResult = businessServiceClient.UnConfirmPaymentFkey(key, Username, Password);
            if (!unconfirmResult.Contains("ERR"))
            {
                return key;
            }
            return "";
        }
        [Authorize]
        public ActionResult PhatHanhHoaDonConLai()
        {


            return View();
        }
        [Authorize]
        public ActionResult TimKiemPhatHanhHoaDonConLai(string chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi, string loaihoadon, string status)
        {
            db = new MTDTEntities();
            if (searchString != null)
            { page = 1; }
            else
            { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;
            chuky = chuky == "0" ? "1" : chuky;
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);

            int pageSize = 10;
            if (pagesize.HasValue)
            {
                pageSize = (int)pagesize;
            }
            ViewBag.pagesize = pageSize;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            //ViewBag.loaihoadon = loaihoadon;
            //ViewBag.httt = httt;
            ViewBag.status = status;
            ViewBag.searchString = searchString;
            //ViewBag.lh28k = lh28k;
            var lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", "", status, 0).ToPagedList(pageNumber, pageSize);
            ViewBag.HDDTs = lst_hd;
            var _lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", "", status, 0);

            decimal thanhTienVs = _lst_hd.Sum(n => n.THANHTIENVS);
            decimal thue = _lst_hd.Sum(n => n.THUE);
            decimal tienVs = _lst_hd.Sum(n => n.TIENVS);
            ViewBag.ProdPrice = tienVs;
            ViewBag.ProdAmount = tienVs;
            ViewBag.Total = tienVs;
            ViewBag.Amount = thanhTienVs;
            ViewBag.VATAmount = thue;

            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            ViewBag.Pattern = Pattern;
            ViewBag.Serial = Serial;

            return View("PhatHanhHoaDonConLai", lst_hd);
        }
        [HttpPost]
        public ActionResult TaoHoaDonDienTu(string CHUKYNO, string Amount, string AmountInWords, string Buyer, string CusAddress, string CusBankNo,
            string CusCode, string CusName, string CusPhone, string CusTaxCode, decimal? DiscountAmount,
            string Name, string PaymentMethod, string PaymentStatus, string Total, string VATAmount, string VATRate,
            string ProdAmount, string ProdName, string ProdUnit, string status)
        {
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            //var soHoaDonPerTask = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "InvoicePerRequest").CONFIG_VALUE);

            Invoice invoice = new Invoice();
            invoice.Amount = Amount;
            invoice.AmountInWords = AmountInWords;
            invoice.ArisingDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
            invoice.CusAddress = CusAddress;
            invoice.CusBankNo = CusBankNo;
            invoice.CusCode = CusCode;
            invoice.CusName = CusName;
            invoice.CusPhone = CusPhone;
            invoice.CusTaxCode = CusTaxCode;
            invoice.DiscountAmount = DiscountAmount.ToString();
            invoice.KindOfService = "Mức 1";
            invoice.PaymentMethod = PaymentMethod;
            invoice.PaymentStatus = PaymentStatus;
            invoice.Products.Add(new Product
            {
                Amount = ProdAmount,
                ProdName = ProdName,
                ProdQuantity = "1",
                ProdUnit = ProdUnit,
            });
            //invoice.ResourceCode = ResourceCode;
            invoice.Total = Total;
            invoice.VATAmount = VATAmount;
            invoice.VATRate = VATRate;
            int chuKyNo = int.Parse(CHUKYNO);
            MTDTEntities dbForCount = new MTDTEntities();
            //int count = db.KHACHHANG_INVOICE.Where(n => n.CHUKYNO == chuKyNo && n.PHAT_HANH_CHUNG == true).Select(n => n.KEY).Distinct().Count();
            var listExist = db.KHACHHANG_INVOICE.Where(n => n.CHUKYNO == chuKyNo && n.PHAT_HANH_CHUNG == true).OrderByDescending(n => n.KEY);
            var lastKey = listExist.Count() == 0 ? "0000000" : listExist.First().KEY;

            int stt = int.Parse(lastKey.Substring(6)) + 1;


            ImportAndPublishInv importAndPublishInv = new ImportAndPublishInv();
            importAndPublishInv.key = CHUKYNO + stt;
            importAndPublishInv.Invoice = invoice;

            xmlInvDataImportAndPublish data = new xmlInvDataImportAndPublish();
            data.Invoices.Add(importAndPublishInv);
            SERVICE_CALL serviceCall = new SERVICE_CALL();
            serviceCall.CALL_TIME = DateTime.Now;
            serviceCall.FUNCTION_NAME = "ImportAndPublishInv";
            serviceCall.XML = data.ToXML();
            db.SERVICE_CALL.Add(serviceCall);
            db.SaveChanges();
            try
            {
                string response = publishServiceClient.ImportAndPublishInv(Account, ACpass, data.ToXML(), Username, Password, Pattern, Serial, 0);
                serviceCall.RESPONSE = response;
                if (!response.Contains("ERR"))
                {
                    string key = "";
                    string no = "";
                    int SOHOADON = -1;
                    string okKeyNo = response.Substring(response.IndexOf('-') + 1);
                    key = okKeyNo.Substring(0, okKeyNo.IndexOf('_'));
                    no = okKeyNo.Substring(okKeyNo.IndexOf('_') + 1);
                    SOHOADON = Int32.Parse(no);
                    INVOICE _invoice = new INVOICE();
                    _invoice.Amount = Decimal.Parse(Amount);
                    _invoice.AmountInwords = AmountInWords;
                    _invoice.ArisingDate = DateTime.Now;
                    _invoice.Buyer = Buyer;
                    _invoice.CusAddress = CusAddress;
                    _invoice.CusBankNo = CusBankNo;
                    _invoice.CusCode = CusCode;
                    _invoice.CusName = CusName;
                    _invoice.CusPhone = CusPhone;
                    _invoice.CusTaxCode = CusTaxCode;
                    _invoice.DiscountAmount = DiscountAmount;
                    _invoice.InvoiceName = Name;
                    _invoice.InvoiceNo = no;
                    _invoice.InvoicePattern = Pattern;
                    _invoice.Key = key;
                    _invoice.KindOfService = "";
                    _invoice.PaymentMethod = PaymentMethod;
                    _invoice.PaymentStatus = 1;
                    _invoice.SerialNo = Serial;
                    _invoice.Total = Decimal.Parse(Total);
                    _invoice.VATAmount = Decimal.Parse(VATAmount);
                    _invoice.VATRate = Decimal.Parse(VATRate);
                    db.INVOICEs.Add(_invoice);
                    db.SaveChanges();
                    //if (listExist.Count() > 0)
                    //{
                    //    List<KHACHHANG_INVOICE> khachHangInvoices = db.KHACHHANG_INVOICE.Where(n=>n.KEY==lastKey).ToList();
                    //    foreach (KHACHHANG_INVOICE khachHangInvoice in khachHangInvoices)
                    //    {
                    //        khachHangInvoice.KEY = key;
                    //        khachHangInvoice.TRANGTHAI = "PUBLISHED";
                    //        db.Entry(khachHangInvoice).State = System.Data.Entity.EntityState.Modified;
                    //    }
                    //}
                    //else
                    //{
                    List<KHACHHANG_INVOICE> khachHangInvoices = new List<KHACHHANG_INVOICE>();
                    var _lst_hd = db.Database.SqlQuery<GetHoaDonJoinLastInvoice>("Exec GetHoaDonJoinLastInvoice @i_searchstring={0}, @i_status ={1}, @i_isLh28k ={2}", "", status, "0");
                    foreach (GetHoaDonJoinLastInvoice hoaDon in _lst_hd)
                    {
                        KHACHHANG_INVOICE khachHangInvoice = new KHACHHANG_INVOICE();
                        khachHangInvoice.CHUKYNO = int.Parse(hoaDon.CHUKYNO);
                        khachHangInvoice.KEY = key;
                        khachHangInvoice.MASOCD = hoaDon.MASOCD;
                        khachHangInvoice.NGAYTAO = DateTime.Now;
                        khachHangInvoice.NGAYUPDATE = DateTime.Now;
                        khachHangInvoice.PHAT_HANH_CHUNG = true;
                        khachHangInvoice.TRANGTHAI = "PUBLISHED";

                        if (khachHangInvoices.SingleOrDefault(n => n.CHUKYNO == khachHangInvoice.CHUKYNO && n.MASOCD == khachHangInvoice.MASOCD) == null)
                            khachHangInvoices.Add(khachHangInvoice);
                    }
                    db.KHACHHANG_INVOICE.AddRange(khachHangInvoices);

                    //}

                }


            }
            catch (Exception ex)
            {
                serviceCall.RESPONSE = ex.Message + " " + ex.StackTrace;
            }
            db.SaveChanges();

            return View("PhatHanhHoaDonConLai");
        }



        [Authorize]
        public ActionResult PhieuBienNhan()
        {
            return View();
        }
        [HttpPost]
        public ActionResult InPhieuBienNhan(string chuky, string searchString, string khuvuc, int duong, string soghi, string loaihoadon, string status, int lh28k)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);
                //db.Database.ExecuteSqlCommand("CreateTabletam @chuky={0}, @khuvuc={1}", chuky, khuvuc);
                string sql = @"exec ReportPhieuBienNhan @i_searchstring = '" + searchString + "',@i_status = '" + status + "',@i_isLh28k = " + lh28k + "";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportPhieuBienNhan.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                paramList.Add(new ReportParameter("thang", thang, false));
                paramList.Add(new ReportParameter("nam", nam, false));
                paramList.Add(new ReportParameter("ngayNhap", DateTime.Now.Day.ToString(), false));
                paramList.Add(new ReportParameter("thangNhap", DateTime.Now.Month.ToString(), false));
                paramList.Add(new ReportParameter("namNhap", DateTime.Now.Year.ToString(), false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                //SqlCommand cmd = new SqlCommand("drop table tam", conn);
                //cmd.ExecuteNonQuery();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }


        [Authorize]
        public ActionResult TimKiemPhieuBienNhan(string chuky, int? page, int? pagesize, string currentFilter, string searchString, string khuvuc, int duong, string soghi, int lh28k)
        {
            searchString = searchString == null ? "" : searchString;
            db = new MTDTEntities();
            ViewBag.CurrentFilter = searchString;
            chuky = chuky == "0" ? "1" : chuky;
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);

            int pageSize = 10;
            if (pagesize.HasValue)
            {
                pageSize = (int)pagesize;
            }
            ViewBag.pagesize = pageSize;
            int pageNumber = (page ?? 1);
            ViewBag.CurrentPage = page;
            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.searchString = searchString;
            ViewBag.lh28k = lh28k;
            IPagedList<TIMKIEMKHACHHANG> lst_hd = null;

            if (lh28k == -1)
            {
                lst_hd = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS >= 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToPagedList(pageNumber, pageSize);
            }

            else if (lh28k == 0)
            {
                lst_hd = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS == 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToPagedList(pageNumber, pageSize);
            }
            else if (lh28k == 1)
            {
                lst_hd = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS > 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToPagedList(pageNumber, pageSize);

            }
            return View("PhieuBienNhan", lst_hd);
        }

        [HttpPost]
        public ActionResult InBienNhanToanBo(string chuky, string khuvuc, int duong, string soghi, int lh28k,string searchString)
        {
            searchString = searchString == null ? "" : searchString;
            db = new MTDTEntities();
            chuky = chuky == "0" ? "1" : chuky;
            db.Database.ExecuteSqlCommand("InsertTimKiemKhachHang @chuky={0}, @khuvuc={1}, @duong={2}, @soghi={3}", chuky, khuvuc, duong, soghi);
            ViewBag.chuky = chuky;
            ViewBag.khuvuc = khuvuc;
            ViewBag.duong = duong;
            ViewBag.soghi = soghi;
            ViewBag.searchString = searchString;
            List<TIMKIEMKHACHHANG> khachHangs = new List<TIMKIEMKHACHHANG>();

            if (lh28k == -1)
            {
                khachHangs = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS >= 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToList();
            }

            else if (lh28k == 0)
            {
                khachHangs = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS == 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToList();
            }
            else if (lh28k == 1)
            {
                khachHangs = db.TIMKIEMKHACHHANGs.Where(n => n.THANHTIENVS > 28000 && n.DANHSO.Contains(searchString)).OrderBy(n => n.DANHSO).ToList();
            }
           
            List<PHIEUBIENNHAN> phieuBienNhanMois = new List<PHIEUBIENNHAN>();
            foreach (var khachHang in khachHangs)
            {
                if(db.PHIEUBIENNHANs.Where(n=>n.CHUKYNO==khachHang.CHUKYNO&&n.MASOCD==khachHang.MASOCD&&khachHang.THANHTIENVS!=0).Count()>0)
                {
                    PHIEUBIENNHAN phieuBienNhan = db.PHIEUBIENNHANs.Where(n => n.CHUKYNO == khachHang.CHUKYNO && n.MASOCD == khachHang.MASOCD).ToList()[0];
                    phieuBienNhanMois.Add(phieuBienNhan);
                }
                else
                {

                    string diaChi = khachHang.SONHA + " " + khachHang.TENSOGHI + ", TP.Cà Mau";
                    string dienThoai = "";
                    int? stt = 0;
                    THONGTINKHACHHANG ttkh = db.THONGTINKHACHHANGs.SingleOrDefault(n => n.MASOCD == khachHang.MASOCD);
                    if (ttkh != null)
                    {
                        if (ttkh.DIA_CHI != null & ttkh.DIA_CHI != "")
                            diaChi = ttkh.DIA_CHI;
                        if (ttkh.SDT != null)
                            dienThoai = ttkh.SDT;
                        stt = ttkh.STT;
                    }
                    PHIEUBIENNHAN phieuBienNhan = new PHIEUBIENNHAN();
                    phieuBienNhan.CHUKYNO = khachHang.CHUKYNO;
                    phieuBienNhan.DIACHI = diaChi;
                    phieuBienNhan.DIENTHOAI = dienThoai;
                    phieuBienNhan.HINHTHUCTHANHTOAN = "Tiền mặt / Chuyển khoản";
                    phieuBienNhan.HOTEN = khachHang.HOTEN;
                    phieuBienNhan.MASOCD = khachHang.MASOCD;
                    phieuBienNhan.MASOKHACHHANG = khachHang.DANHSO;
                    phieuBienNhan.MASOTHUE = khachHang.MASOTHUE;
                    phieuBienNhan.SOTIEN = khachHang.THANHTIENVS;
                    phieuBienNhan.SOTIENBANGCHU = NumberToTextVN(khachHang.THANHTIENVS);
                    phieuBienNhan.STT = stt;
                    if (khachHang.THANHTIENVS > 0)
                    {
                        phieuBienNhanMois.Add(phieuBienNhan);
                        db.PHIEUBIENNHANs.Add(phieuBienNhan);
                        db.SaveChanges();
                    }
                }
                
            }

            return ViewInPhieuBienNhan(phieuBienNhanMois.OrderBy(n=>n.ID_PHIEU_BIEN_NHAN).ToList());
        }

        public ActionResult ViewInPhieuBienNhan(List<PHIEUBIENNHAN> phieuBienNhans)
        {
            try
            {
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportPhieuBienNhan.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetPhieuBienNhan", phieuBienNhans));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = phieuBienNhans[0].CHUKYNO.Substring(4, 2);
                var nam = phieuBienNhans[0].CHUKYNO.Substring(0, 4);
                paramList.Add(new ReportParameter("thang", thang, false));
                paramList.Add(new ReportParameter("nam", nam, false));
                paramList.Add(new ReportParameter("ngayNhap", DateTime.Now.Day.ToString(), false));
                paramList.Add(new ReportParameter("thangNhap", DateTime.Now.Month.ToString(), false));
                paramList.Add(new ReportParameter("namNhap", DateTime.Now.Year.ToString(), false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View("InPhieuBienNhan");
        }



        public ActionResult TaiHoaDon(string fkey)
        {
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            string result = "";
            var khachHangInvoice = db.KHACHHANG_INVOICE.SingleOrDefault(n => n.KEY == fkey);
            if (khachHangInvoice != null)
            {
                if (khachHangInvoice.TRANGTHAI == "PUBLISHED")
                {
                    result = portalServiceSoapClient.downloadInvPDFFkeyNoPay(fkey, Username, Password);
                }
                if (khachHangInvoice.TRANGTHAI == "CONFIRMED")
                {
                    result = portalServiceSoapClient.downloadInvPDFFkey(fkey, Username, Password);
                }
            }
            if (result == "ERR:6")
            {
                return null;
            }

            byte[] bytes = Convert.FromBase64String(result);

            MemoryStream pdfStream = new MemoryStream();
            pdfStream.Write(bytes, 0, bytes.Length);
            pdfStream.Position = 0;
            return new FileStreamResult(pdfStream, "application/pdf");

        }
        public ActionResult TaoHoaDonThayThe(string fkey)
        {
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            ViewBag.Pattern = Pattern;
            ViewBag.Serial = Serial;
            ViewBag.Fkey = fkey;
            INVOICE invoice = db.INVOICEs.SingleOrDefault(n => n.Key == fkey);
            ViewBag.Invoice = invoice;
            return View(invoice);
        }
        public ActionResult TaoMoiHoaDonThayThe(string fkey, string Amount, string AmountInWords, string Buyer, string CusAddress, string CusBankNo,
            string CusCode, string CusName, string CusPhone, string CusTaxCode, decimal? DiscountAmount,
            string Name, string PaymentMethod, string PaymentStatus, string Total, string VATAmount, string VATRate,
            string ProdAmount, string ProdName, string ProdUnit)
        {
            Account = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Account").CONFIG_VALUE;
            ACpass = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "ACpass").CONFIG_VALUE;
            Username = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Username").CONFIG_VALUE;
            Password = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Password").CONFIG_VALUE;
            Pattern = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Pattern").CONFIG_VALUE;
            Serial = db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "Serial").CONFIG_VALUE;
            //var soHoaDonPerTask = int.Parse(db.CONFIGs.SingleOrDefault(n => n.CONFIG_NAME == "InvoicePerRequest").CONFIG_VALUE);


            ReplaceInv _invoice = new ReplaceInv();
            _invoice.key = fkey + "R";
            _invoice.Amount = Amount;
            _invoice.AmountInWords = AmountInWords;
            _invoice.ArisingDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
            _invoice.CusAddress = CusAddress;
            _invoice.CusBankNo = CusBankNo;
            _invoice.CusCode = CusCode;
            _invoice.CusName = CusName;
            _invoice.CusPhone = CusPhone;
            _invoice.CusTaxCode = CusTaxCode;
            _invoice.DiscountAmount = DiscountAmount.ToString();
            _invoice.KindOfService = "Mức 1";
            _invoice.PaymentMethod = PaymentMethod;
            _invoice.PaymentStatus = PaymentStatus;
            _invoice.Products.Add(new Product
            {
                Amount = ProdAmount,
                ProdName = ProdName,
                ProdQuantity = "1",
                ProdUnit = ProdUnit,
            });
            //invoice.ResourceCode = ResourceCode;
            _invoice.Total = Total;
            _invoice.VATAmount = VATAmount;
            _invoice.VATRate = VATRate;
            xmlInvDataReplace xml = new xmlInvDataReplace();
            xml.ReplaceInv = _invoice;
            string result = businessServiceClient.replaceInv(Account, ACpass, xml.ToXML(), Username, Password, fkey, 0);
            SERVICE_CALL serviceCall = new SERVICE_CALL();
            serviceCall.CALL_TIME = DateTime.Now;
            serviceCall.FUNCTION_NAME = "replaceInv";
            serviceCall.XML = xml.ToXML();
            db.SERVICE_CALL.Add(serviceCall);
            db.SaveChanges();
            if (result.Contains("ERR"))
            {
                serviceCall.RESPONSE = result;
                db.SaveChanges();
            }
            else
            {
                serviceCall.RESPONSE = result;
                string soHoaDon = result.Substring(result.IndexOf('-') + 1);
                db.SaveChanges();
                List<KHACHHANG_INVOICE> khachHangInvoices = db.KHACHHANG_INVOICE.Where(n => n.KEY == fkey).ToList();
                foreach (var khachHangInvoice in khachHangInvoices)
                {
                    khachHangInvoice.TRANGTHAI = "REPLACED";
                    khachHangInvoice.NGAYUPDATE = DateTime.Now;
                    db.Entry(khachHangInvoice).State = System.Data.Entity.EntityState.Modified;
                    KHACHHANG_INVOICE _khachHangInvoice = new KHACHHANG_INVOICE();
                    _khachHangInvoice.KEY = _invoice.key;
                    _khachHangInvoice.MASOCD = khachHangInvoice.MASOCD;
                    _khachHangInvoice.NGAYTAO = DateTime.Now;
                    _khachHangInvoice.NGAYUPDATE = DateTime.Now;
                    _khachHangInvoice.PHAT_HANH_CHUNG = khachHangInvoice.PHAT_HANH_CHUNG;
                    _khachHangInvoice.SO_HOA_DON = int.Parse(soHoaDon);
                    db.KHACHHANG_INVOICE.Add(_khachHangInvoice);
                }
            }
            db.SaveChanges();
            return View();
        }
        public ActionResult CauHinhHDDT()
        {
            List<CONFIG> configs = db.CONFIGs.ToList();
            return View(configs);
        }
        public string CapNhatThamSo(int id, string giaTri)
        {
            CONFIG config = db.CONFIGs.SingleOrDefault(n => n.ID == id);
            config.CONFIG_VALUE = giaTri;
            db.Entry(config).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges().ToString();
        }
        #region Đọc tiền thành chữ
        public String NumberToTextVN(decimal? total)
        {
            try
            {
                string rs = "";
                total = Math.Round(total.Value, 0);
                string[] ch = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
                string[] rch = { "lẻ", "mốt", "", "", "", "lăm" };
                string[] u = { "", "mươi", "trăm", "ngàn", "", "", "triệu", "", "", "tỷ", "", "", "ngàn", "", "", "triệu" };
                string nstr = total.ToString();

                int[] n = new int[nstr.Length];
                int len = n.Length;
                for (int i = 0; i < len; i++)
                {
                    n[len - 1 - i] = Convert.ToInt32(nstr.Substring(i, 1));
                }

                for (int i = len - 1; i >= 0; i--)
                {
                    if (i % 3 == 2)// số 0 ở hàng trăm
                    {
                        if (n[i] == 0 && n[i - 1] == 0 && n[i - 2] == 0) continue;//nếu cả 3 số là 0 thì bỏ qua không đọc
                    }
                    else if (i % 3 == 1) // số ở hàng chục
                    {
                        if (n[i] == 0)
                        {
                            if (n[i - 1] == 0) { continue; }// nếu hàng chục và hàng đơn vị đều là 0 thì bỏ qua.
                            else
                            {
                                rs += " " + rch[n[i]]; continue;// hàng chục là 0 thì bỏ qua, đọc số hàng đơn vị
                            }
                        }
                        if (n[i] == 1)//nếu số hàng chục là 1 thì đọc là mười
                        {
                            rs += " mười"; continue;
                        }
                    }
                    else if (i != len - 1)// số ở hàng đơn vị (không phải là số đầu tiên)
                    {
                        if (n[i] == 0)// số hàng đơn vị là 0 thì chỉ đọc đơn vị
                        {
                            if (i + 2 <= len - 1 && n[i + 2] == 0 && n[i + 1] == 0) continue;
                            rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
                            continue;
                        }
                        if (n[i] == 1)// nếu là 1 thì tùy vào số hàng chục mà đọc: 0,1: một / còn lại: mốt
                        {
                            rs += " " + ((n[i + 1] == 1 || n[i + 1] == 0) ? ch[n[i]] : rch[n[i]]);
                            rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
                            continue;
                        }
                        if (n[i] == 5) // cách đọc số 5
                        {
                            if (n[i + 1] != 0) //nếu số hàng chục khác 0 thì đọc số 5 là lăm
                            {
                                rs += " " + rch[n[i]];// đọc số 
                                rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị
                                continue;
                            }
                        }
                    }

                    rs += (rs == "" ? " " : ", ") + ch[n[i]];// đọc số
                    rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị
                }
                if (rs[rs.Length - 1] != ' ')
                    rs += " đồng";
                else
                    rs += "đồng";

                if (rs.Length > 2)
                {
                    string rs1 = rs.Substring(0, 2);
                    rs1 = rs1.ToUpper();
                    rs = rs.Substring(2);
                    rs = rs1 + rs;
                }
                return rs.Trim().Replace("lẻ,", "lẻ").Replace("mươi,", "mươi").Replace("trăm,", "trăm").Replace("mười,", "mười");
            }
            catch
            {
                return "";
            }

        }
        #endregion
        public ActionResult BangKeHoaDon()
        {
            return View();
        }
        public ActionResult InBangKeHoaDon(string chuky,string khuvuc)
        {
            try
            {
                var conn = Business.Connection.ConnectionString();
                conn.Open();
                string sql = @"exec ReportBangKeHoaDon @chukyno = '" + chuky+"'"  + ", @khuvuc = '" + khuvuc + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);//lấy dữ liệu từ DB bằng lệnh sql thông qua kết nối con ở trên               
                DataTable dt = new DataTable();//tạo table dt để chứa dữ liệu
                da.Fill(dt);//đổ dữ liệu lấy được vào Table dt mới tạo này                
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.PageCountMode = PageCountMode.Actual;
                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Content\ReportBangKeHoaDon.rdlc";
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));

                ViewBag.ReportViewer = reportViewer;
                /**/
                List<ReportParameter> paramList = new List<ReportParameter>();
                var thang = chuky.Substring(4, 2);
                var nam = chuky.Substring(0, 4);
                var kv = (from a in db.KHUVUCs where a.MAKHUVUC == khuvuc select a.TENKHUVUC).SingleOrDefault().ToString().ToUpper();
                paramList.Add(new ReportParameter("TENKHUVUC", kv, false));
                paramList.Add(new ReportParameter("THANG", thang, false));
                paramList.Add(new ReportParameter("NAM", nam, false));
                reportViewer.LocalReport.SetParameters(paramList);
                /**/
                reportViewer.LocalReport.Refresh();
                conn.Close();//đóng kết nối
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return View();
        }

    }
    class SelectedHoaDon
    {
        public string chuKyNo { get; set; }
        public string msCoDinh { get; set; }
        public SelectedHoaDon(string chuKyNo, string msCoDinh)
        {
            this.chuKyNo = chuKyNo;
            this.msCoDinh = msCoDinh;
        }
    }
}